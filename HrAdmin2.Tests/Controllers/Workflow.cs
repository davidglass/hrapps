﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HrAdmin2.Controllers.Api;
using HrAdmin2.Models;
using System.Net.Http;
using System.Net;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Tests.Controllers
{
    [TestClass]
    public class Workflow
    {
        [TestMethod]
        public void ApprovalWorkflowCreation()
        {
            var repo = new ApprovalWorkflowController();

            int t = repo.CreateApprovalWorkflow(ApprovalWorkflow.approvalApp.PAR, 5247, 6058); //5247, 6058
            Assert.AreNotEqual(t, -1);
        }

        [TestMethod]
        public void ApprovalVmList()
        {
            //ApprovalWorkflowVm workflow = new ApprovalWorkflowVm(43);
            //ApprovalVm approval = new ApprovalVm(6084);//vacant
            ApprovalVm approval = new ApprovalVm(5548); //curtis
            //ApprovalVm approval = new ApprovalVm(5219); //cristy
            Assert.AreEqual(approval.ParApprovals.Count, approval.ParApprovals.Count);
        }

        [TestMethod]
        public void ApprovalVm()
        {
            ApprovalWorkflowVm workflow = new ApprovalWorkflowVm(21);
            Assert.AreEqual(workflow.Workflow.ApprovalTasks.Count, workflow.Tasks.Count);
        }

        //[TestMethod]
        //public void ApprovalTaskUpdate()
        //{
        //    var taskCon = new ApprovalTaskController();
        //    var workflowCon = new ApprovalWorkflowController();

        //    int workflowId = 22;
        //    int taskNo = 2;

        //    ApprovalWorkflow workflow = workflowCon.Get(workflowId);
        //    ApprovalTask updateTask = workflow.ApprovalTasks[taskNo];
        //    updateTask.Comment = "update in unit test at " + DateTime.Now.ToString();
        //    //updateTask.StatusCode = ApprovalWorkflow.statusCode.Canceled;
        //    updateTask.StatusCode = ApprovalWorkflow.statusCode.Active;
        //    //updateTask.StatusCode = ApprovalWorkflow.statusCode.Rejected;
        //    updateTask.StatusCode = ApprovalWorkflow.statusCode.Approved;
        //    updateTask.FinalApproverPersonId = 6058;
        //    //updateTask.CompletedDate = DateTime.Now; //not needed

        //    var response = taskCon.Put(updateTask);

        //    ApprovalWorkflow workflowSaved = workflowCon.Get(workflowId);
        //    ApprovalTask taskSaved = workflowSaved.ApprovalTasks[taskNo];

        //    Assert.AreEqual(updateTask.Comment, taskSaved.Comment);
        //    Assert.AreEqual(updateTask.StatusCode, taskSaved.StatusCode);

        //}

        [TestMethod]
        public void ApprovalDelegateLife()
        {
            //test create
            var repo = new ApprovalDelegateController();
            ApprovalDelegate dele = new ApprovalDelegate()
            {
                DelegatedPositionId = 5247,
                DelegatedToPersonId = 6058,
                StartDate = DateTime.Now,
                EndDate = new DateTime(2013,11,16)
            };
            dele.Apps.Add(new ApprovalApp() { Id = 1 });
            dele.Apps.Add(new ApprovalApp() { Id = 2 });
            int id = repo.Post(dele).Delegate.Id;
            Assert.AreNotEqual(id, -1);

            //test get
            ApprovalDelegate delegateGet = repo.Get(id).Delegate;
            Assert.AreEqual(dele.DelegatedPositionId, delegateGet.DelegatedPositionId);
            Assert.AreEqual(dele.StartDate.DayOfYear, delegateGet.StartDate.DayOfYear);
            Assert.AreEqual(dele.EndDate.DayOfYear, delegateGet.EndDate.DayOfYear);
            Assert.AreEqual(dele.DelegatedToPersonId, delegateGet.DelegatedToPersonId);
            Assert.AreEqual(dele.Apps.Count, delegateGet.Apps.Count);


            //test delete
            var response = repo.Delete(id);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }
    }
}
