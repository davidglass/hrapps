﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    // TODO: consider converting Admin tools to SPA (Single Page App)
    public class CostCenterController : BaseMvcController
    {
        public ActionResult Index()
        {
            var ccvm = new CostCenterListVm
            {
                CostCenters = db.GetByProc<CostCenterRow>("GetCostCenters", new {}),
                CurrentUser = ui,
                EmpSelection = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", new {})
            };
            ccvm.EmpSelection.Insert(0, new SelectListItem { Value = "", Text = " - select - " });
            return View("~/Views/Admin/CostCenter.cshtml", ccvm);
        }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class CostCenterListVm : BaseVm
    {
        public List<SelectListItem> EmpSelection { get; set; }
        public List<CostCenterRow> CostCenters { get; set; }
    }

    public class CostCenterRow : ModelBase
    {
        public bool IsActive { get; set; }
        public string DescShort { get; set; }
        public string DescLong { get; set; }
        public int Positions { get; set; }
        public int Vacancies { get; set; }
        public int ManagerEmpId { get; set; }
        public string ManagerName { get; set; }
    }
}