﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using NLog;

using System.Web.Configuration;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using System.Web.Helpers;
using HrAdmin2.Filters;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System.Net.Mail;
using System.IO;

using RazorEngine;

using System.Web.Mvc;
using ActionName = System.Web.Http.ActionNameAttribute;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    public class ChecklistController : BaseApiController
    {

        public HttpResponseMessage Put(ChecklistTable clvm)
        {
            clvm.UpdateBy = ui.UserId;
            clvm.UpdateDt = DateTime.Now;

            try
            {
                db.Checklists.Attach(clvm);
                var e = db.Entry<ChecklistTable>(clvm);
                e.State = System.Data.EntityState.Modified;
                // currently this is the only editable property...
                //e.Property("IsActive").IsModified = true;
                var updated = db.SaveChanges();
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Post(ChecklistTable ck)
        {
            try
            {
                db.Checklists.Add(ck);
                int saved = db.SaveChanges();
                hrm.Content = new StringContent(Json.Encode(ck));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                        new
                        {
                            error = uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                        }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.InnerException.GetType().ToString() + ":" + xcp.InnerException.InnerException.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        [ActionName("Email")]
        public HttpResponseMessage PostEmail(int id, List<string> CCL)
        {
            try
            {
                // TODO: create separate, email-specific View / template.
                Checklist checklistDetail = db.GetSingleByProc<Checklist>("GetChecklistDetail", parameters: new { ParId = id, LanId = ui.LanId });
                var ckvm = new ChecklistVm
                {
                    Empchecklist = checklistDetail,
                    ViewMode = "read" // TODO: make this enum...
                };

                var approot = HttpContext.Current.Server.MapPath("~");
                var template = File.ReadAllText(String.Format("{0}/Views/Checklist/EmailTemplate.cshtml", approot));
                var rslt = Razor.Parse(template, ckvm);

                // TODO: checkbox for [] send me a copy, appending to Bcc list.
                var sc = new SmtpClient(WebConfigurationManager.AppSettings["smtpServer"]);
                var mFrom = new MailAddress("no-reply@cts.wa.gov", "CTS Checklist System");
                var mBccString = WebConfigurationManager.AppSettings["checklistEmailBcc"];
                string[] mBccList = mBccString.Split(new char[] { ',' });

                var mTo = new MailAddress(ui.EmailAddress);

                //var mTo = new MailAddress(WebConfigurationManager.AppSettings["checklistEmailTo"]
                //    , WebConfigurationManager.AppSettings["checklistEmailToName"]);
                var mm = new MailMessage(mFrom, mTo)
                {
                    IsBodyHtml = true,
                    Subject = String.Format("CTS Checklist #{0}: {1}",
                        checklistDetail.ParId, checklistDetail.Name),
                    Body = rslt
                };
                foreach (var cc in CCL) {
                    mm.CC.Add(new MailAddress(cc));
                }
                for (int h = 0; h < mBccList.Length; h++)
                {
                    mm.Bcc.Add(new MailAddress(mBccList[h]));
                }
                sc.Send(mm);

                var sent = new ChecklistEmail
                {
                    
                    HtmlBody = mm.Body,
                    ParId = id,
                    TimeSent = DateTime.Now,
                    SentBcc = AddressesToCsv(mm.Bcc),
                    SentFrom = mm.From.Address,
                    SentTo = mm.To[0].Address,
                    //SentCC = mm.CC[0].Address
                };
                //db.ChecklistEmails.Add(sent);
                //var saveCount = db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            // TODO: add logging to exception handling...
            catch (SmtpException mxcp)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(Json.Encode(
                        new
                        {
                            error = "email failed: " + mxcp.Message
                        }))
                };
            }
            catch (Exception xcp)
            {

                var lei = new LogEventInfo(LogLevel.Error, logger.Name, "error updating approval task.");
                //lei.SetStackTrace(new System.Diagnostics.StackTrace(xcp), 0);
                //lei.Properties.Add("ErrorSource", xcp.Source);
                lei.Properties.Add("ErrorSource", logger.Name);
                lei.Properties.Add("ErrorClass", xcp.GetType().ToString());
                lei.Properties.Add("ErrorMethod", xcp.TargetSite.Name);
                lei.Properties.Add("ErrorMessage", xcp.Message);
                if (xcp.InnerException != null)
                {
                    lei.Properties.Add("InnerErrorMessage", xcp.InnerException.Message);
                }
                lei.Properties.Add("StackTrace", xcp.StackTrace.ToString());
                logger.Log(lei);


                logger.Log(NLog.LogLevel.Error, "email failed: " + xcp.Message);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(Json.Encode(
                        new
                        {
                            error = "email failed: " + xcp.Message
                        }))
                };
            }
        }

        private string AddressesToCsv(MailAddressCollection addressList)
        {
            var csv = new string[addressList.Count];
            int i = 0;
            foreach (MailAddress ma in addressList)
            {
                csv[i++] = ma.Address;
            }
            return String.Join(",", csv);
        }

    }
}
