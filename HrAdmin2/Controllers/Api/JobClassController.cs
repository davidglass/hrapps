﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HrAdmin2.Models;
using System.Web.Helpers;

namespace HrAdmin2.Controllers.Api
{
    public class JobClassController : BaseApiController
    {
//        public JobClass Post(JobClass p)
        public HttpResponseMessage Post(JobClass p)
        {
            try
            {
                //throw new Exception("Something went terribly wrong!!!");
                db.JobClasses.Add(p);
                int saved = db.SaveChanges(); // attaches newly-generated Id to p
                hrm.Content = new StringContent(Json.Encode(p));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                        new
                        {
                            error = uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                        }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.InnerException.GetType().ToString() + ":" + xcp.InnerException.InnerException.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Delete(int id)
        {
            try {
                db.Exec("DeleteJobClass", new { Id = id });
                hrm.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Put(JobClass jc)
        {
            try
            {
                db.JobClasses.Attach(jc);
                var e = db.Entry<JobClass>(jc);
                e.Property("IsActive").IsModified = true;
                var updated = db.SaveChanges();
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}