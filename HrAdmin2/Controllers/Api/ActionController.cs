﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers.Api
{
    public class ActionController : ApiController
    {
        private HrAdminDbContext db = new HrAdminDbContext();
        //GET api/Action/u5
        // TODO: change this to List<SelectListItem>, with Text = " - select - ", Value="" item prepended
        //public List<ActionReason> GetApptActionList(string id) // id is HRMS ActionCode: u0, u3, etc.
        //{
        //    var alist = db.GetByProc<ActionReason>("GetActionReasons", parameters: new { ActionCode = id });
        //    alist.Insert(0, new ActionReason() { Id = 0, Reason = " - select - " });
        //    return alist;
        //}
        public List<SelectListItem> GetApptActionList(string id) // id is HRMS ActionCode: u0, u3, etc.
        {
            //var alist = db.GetByProc<ActionReason>("GetActionReasons", parameters: new { ActionCode = id });
            //alist.Insert(0, new ActionReason() { Id = 0, Reason = " - select - " });
            var alist = db.GetByProc<SelectListItem>("GetActionReasons", parameters: new { ActionCode = id });
            ParSelectors.PrependBlankSelection(alist);
            return alist;
        }
    }
}
