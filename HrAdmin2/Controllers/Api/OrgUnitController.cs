﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers.Api
{
    // TODO: auth attributes, pass userid to procs
    public class OrgUnitController : BaseApiController
    {
        //private HrAdminDbContext db = new HrAdminDbContext();

        // GetOrgUnit is no longer being used...initial tree data is loaded in other (Mvc) OrgUnitController.
        // GET api/OrgUnit
        //public OrgNodeJsTree GetOrgTree()
        //{
        //    // hardcoded root and user for testing:
        //    var orgTreeVm = new OrgNodeJsTree(
        //        db.GetByProc<OrgNode>("GetOrgStructure", parameters: new {
        //            RootId = 31008202,
        //            UserId = 1
        //        })
        //    );
        //    return orgTreeVm;
        //}

        public OrgUnit Get(int id)
        {
            return db.OrgUnits.Find(id);
        }

        public List<OrgNodeJsTree> GetLazyExpand(int id)
        {
            // first expand lazy target node:
            db.Exec("ExpandUserOrgUnitNode",
                parameters: new
                {
                    NodeId = id,
                    //                            AppUserId = AppUserId(),
                    //AppUserId = 1,
                    LanId = User.Identity.Name,
                    IncludeSubNodes = false
                }
            );

            // TODO: consider Repository as injectable controller dependency...
            // hardcoded root and user for testing:
            var tree = new OrgNodeJsTree(
                db.GetByProc<OrgNode>("GetOrgStructure", parameters: new
                {
                    RootId = id,
                    LanId = User.Identity.Name
                    //UserId = 1
                })
            );
             return tree.children;
        }

        public List<OrgNodeJsTree> GetLazyExpandAll(int id)
        {
            // first expand lazy target node:
            db.Exec("ExpandUserOrgUnitNode",
                parameters: new
                {
                    NodeId = id,
                    //                            AppUserId = AppUserId(),
                    //AppUserId = 1,
                    LanId = User.Identity.Name,
                    IncludeSubNodes = true // should be only difference from GetLazyExpand.
                }
            );

            // TODO: consider Repository as injectable controller dependency...
            // hardcoded root and user for testing:
            var tree = new OrgNodeJsTree(
                db.GetByProc<OrgNode>("GetOrgStructure", parameters: new
                {
                    RootId = id,
                    LanId = User.Identity.Name
                    //UserId = 1
                })
            );
            return tree.children;
        }

        // GET api/OrgUnit/5
        //public OrgNode GetOrgNode(int id)
        //{
        //    OrgNode orgnode = db.OrgNodes.Find(id);
        //    if (orgnode == null)
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
        //    }

        //    return orgnode;
        //}

        // PUT api/node/5
        //[ActionName("NodeOp")]
        public void Put(int id, UserOrgNode n) // NOTE, formatter that converts POST body to Node omits URL params! (id)
        {
            switch (n.action)
            {
                case "collapse":
                    // TODO: error handling, logging, etc.
                    db.Exec("CollapseUserOrgUnitNode",
                        parameters: new
                        {
                            NodeId = id,
                            //AppUserId = AppUserId(),
                            //AppUserId = 1,
                            LanId = User.Identity.Name,
                            IncludeSubNodes = false
                        }
                    );
                    return;
                case "collapse_all":
                    // TODO: error handling, logging, etc.
                    db.Exec("CollapseUserOrgUnitNode",
                        parameters: new
                        {
                            NodeId = id,
                            //AppUserId = AppUserId(),
                            //AppUserId = 1,
                            LanId = User.Identity.Name,
                            IncludeSubNodes = true
                        }
                    );
                    return;
                case "expand":
                    var result = 
                    db.Exec("ExpandUserOrgUnitNode",
                        parameters: new
                        {
                            NodeId = id,
//                            AppUserId = AppUserId(),
//                            AppUserId = 1,
                            LanId = User.Identity.Name,
                            IncludeSubNodes = false
                        }
                    );
                    return;
                default: return;
            }
        }
        /* template version:
        // PUT api/OrgUnit/5
        public HttpResponseMessage PutOrgNode(int id, OrgNode orgnode)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != orgnode.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(orgnode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
        */

        //// POST api/OrgUnit
        //public HttpResponseMessage PostOrgNode(OrgNode orgnode)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.OrgNodes.Add(orgnode);
        //        db.SaveChanges();

        //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, orgnode);
        //        response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = orgnode.Id }));
        //        return response;
        //    }
        //    else
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }
        //}

        //// DELETE api/OrgUnit/5
        //public HttpResponseMessage DeleteOrgNode(int id)
        //{
        //    OrgNode orgnode = db.OrgNodes.Find(id);
        //    if (orgnode == null)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.NotFound);
        //    }

        //    db.OrgNodes.Remove(orgnode);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, orgnode);
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}