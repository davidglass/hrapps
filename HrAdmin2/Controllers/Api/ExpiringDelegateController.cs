﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers.Api
{
    public class ExpiringDelegateController : BaseApiController
    {
        public List<ExpiringDelegate> Get()
        {
            return db.GetByProc<ExpiringDelegate>("GetExpiringDelegates", new { });
        }
    }
}
