﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using RazorEngine;
using System.IO;

namespace HrAdmin2.Controllers.Api
{
    public class ApprovalDelegateController : BaseApiController
    {
        //private HrAdminDbContext db = new HrAdminDbContext();

//        // GET api/ApprovalDelegates
//        public IEnumerable<ApprovalDelegate> Get()
//        {
//            try
//            {
//                return GetUnExpiredDelegates(((ControllerContext.Request.Properties["MS_HttpContext"] as HttpContextWrapper).Profile as IProfileUser).EmployeeId.Value);
//            }
//            catch
//            {
//                //TODO: how to use Request.CreateResponse to include response header as well?
//                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest));
//            }
//        }

        // GET api/ApprovalDelegates/5
        public ApprovalDelegateVm Get(int id)
        {
            return GetVm(id);
        }

        //Currently create and delete only
        // PUT api/ApprovalDelegates/5
        //public HttpResponseMessage Put(ApprovalDelegate approvaldelegate);


       
        // POST api/ApprovalDelegate
        //[HrAdmin2.Filters.ValidationActionFilter] //broke when removing require from EndDate, TODO: look into
        public ApprovalDelegateVm Post(ApprovalDelegate approvalDelegate)
        {
            
            try
            {

                int newId;

                int? UserId = null;
                if (!String.IsNullOrEmpty(ui.LanId))
                {
                    //lanId set, assign id
                    Employee e = db.GetByProc<Employee>("GetEmployeeIdOfLanId",
                        parameters: new { lanId = ui.LanId }).First();
                    if (e != null) UserId = e.Id;
                }

                if (approvalDelegate.Indefinite)
                {
                    //no end date
                    newId = db.GetSingleByProc<ApprovalDelegate>("CreateApprovalDelegate",
                        new
                        {
                            DelegatedToPersonId = approvalDelegate.DelegatedToPersonId,
                            DelegatedPositionId = approvalDelegate.DelegatedPositionId,
                            StartDate = approvalDelegate.StartDate,
                            Indefinite = approvalDelegate.Indefinite,
                            CreatorPersonId = UserId,
                            CreatedDate = DateTime.Now
                        }).Id;
                }
                else
                {
                    newId = db.GetSingleByProc<ApprovalDelegate>("CreateApprovalDelegate",
                        new
                        {
                            DelegatedToPersonId = approvalDelegate.DelegatedToPersonId,
                            DelegatedPositionId = approvalDelegate.DelegatedPositionId,
                            StartDate = approvalDelegate.StartDate,
                            EndDate = approvalDelegate.EndDate,
                            Indefinite = approvalDelegate.Indefinite,
                            CreatorPersonId = UserId,
                            CreatedDate = DateTime.Now
                        }).Id;
                }

                //if no apps, assume all active apps
                if (approvalDelegate.Apps.Count == 0)
                {
                    db.Exec("CreateApprovalDelegateAppLinkAllActive",
                    new
                    {
                        DelegateId = newId
                    });
                }
                else
                {
                    foreach (ApprovalApp app in approvalDelegate.Apps)
                    {
                        db.Exec("CreateApprovalDelegateAppLink",
                        new
                        {
                            DelegateId = newId,
                            AppId = app.Id
                        });
                    }
                }

                var vm = GetVm(newId);
                EmailNotice(vm, "Created");

                return vm;

            }
            catch (Exception xcp)
            {
                throw new Exception("There was a server error for Delegate Creation.  Please try again later.");
            }
            
        }

        // DELETE api/ApprovalDelegates/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {

                int? UserId = null;
                if (!String.IsNullOrEmpty(ui.LanId))
                {
                    //lanId set, assign id
                    Employee e = db.GetByProc<Employee>("GetEmployeeIdOfLanId",
                        parameters: new { lanId = ui.LanId }).First();
                    if (e != null) UserId = e.Id;
                }

                if (!db.Exec("DeleteApprovalDelegate", new { Id = id, DeletePersonId = UserId, DeleteDate = DateTime.Now }))
                {
                    throw new Exception("DeleteApprovalDelegate failed.");
                }
                
                EmailNotice(GetVm(id), "Deleted");
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "delete failed."
                };
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }


        #region Delegate Repository


        private ApprovalDelegateVm GetVm(int id)
        {
            try
            {
                //get delegate data
                ApprovalDelegate dele = db.GetSingleByProc<ApprovalDelegate>("GetApprovalDelegate",
                    parameters: new { Id = id });

                //get app list
                dele.Apps = db.GetByProc<ApprovalApp>("GetApprovalDelegateApps",
                    parameters: new { DelegateId = id });

                var vm = new ApprovalDelegateVm(dele, db);

                return vm;

            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
        }

        private void EmailNotice(ApprovalDelegateVm delegateVm, string message)
        {
            try
            {
                ApprovalDelegateEmailVm vm = new ApprovalDelegateEmailVm(delegateVm);
                vm.CreatedOrDeleted = message;
                

                //set Urul path
                var fullUrl = HttpContext.Current.Request.Url.ToString();
                var urlPath = fullUrl.Substring(0, fullUrl.LastIndexOf("/api/") + 1);
                vm.UrlPath = urlPath;

                var approot = HttpContext.Current.Server.MapPath("~");
                var template = File.ReadAllText(String.Format("{0}/Views/Delegation/EmailNotificationDelegate.cshtml", approot));

                string body = Razor.Parse(template, vm);
                string subject = String.Format("{0}, {1} delegates HR Apps approvals to {2}", vm.CreatedOrDeleted, vm.DelegatedPositionPersonLastName + ": " + vm.DelegatedPositionTitle, vm.DelegatedToPreferredName + " " + vm.DelegatedToLastName);
                string fromAddress = "no-reply@cts.wa.gov";
                string fromDisplayName = "CTS PAR System";


                //Send to Delegated to person
                db.Exec("CreateEmail", parameters: new
                {
                    fromAddress = fromAddress,
                    fromDisplayName = fromDisplayName,
                    toAddress = vm.DelegatedToEmail,
                    toDisplayName = vm.DelegatedToLastName + ", " + vm.DelegatedToPreferredName,
                    body = body,
                    subject = subject,
                    isBodyHtml = true,
                    source = "ApprovalDelegate",
                    sourceId = delegateVm.Delegate.Id
                });

                //Send to delegated position if employee in position
                if (vm.DelegatedPositionPersonId.HasValue)
                {
                    db.Exec("CreateEmail", parameters: new
                    {
                        fromAddress = fromAddress,
                        fromDisplayName = fromDisplayName,
                        toAddress = vm.DelegatedPositionEmail,
                        toDisplayName = vm.DelegatedPositionPersonLastName + ", " + vm.DelegatedPositionPersonPreferredName,
                        body = body,
                        subject = subject,
                        isBodyHtml = true,
                        source = "ApprovalDelegate",
                        sourceId = delegateVm.Delegate.Id
                    });
                }

            }
            catch { };
        }



//        private IEnumerable<ApprovalDelegate> GetByAppId(int appId)
//        {
//            //the following code is same as 
//            //this.Delegates.Where(s => s.Apps.Any(c => c.Id == appId));
//            return this.Get<ApprovalDelegate>(s => s.Apps.Any(c => c.Id == appId));
//        }



//        /// <summary>
//        /// Get list of all active delegates for the current personID in question 
//        /// (could be current logged in or by a HR Admin to filter all delegates created by an user)
//        /// active delegate are the ones that has not ended, but has already started.
//        /// </summary>
//        /// <returns>list of all active delegates</returns>
//        private List<ApprovalDelegate> GetActiveDelegates(int personId)
//        {
//            DateTime dt = DateTime.Today.Date;
//            List<ApprovalDelegate> list = this.ApprovalDelegates
//                .Include("Position")
//                .Include("Person")
//                .Where(x => x.Position.OrganizationalAssignments.Any(y => y.PersonId == personId) && (x.EndDate >= dt) && (x.StartDate <= dt))
//                .OrderBy(x => x.StartDate).ToList();
//            return list;

//        }

//        private ApprovalDelegateCreateFormDto GetCreateFormDto()
//        {
//            //how to track changes if we happen to track changes when ProxyCreationEnabled is false
//            //http://msdn.microsoft.com/en-us/library/vstudio/dd456854(v=vs.100).aspx
//            this.Configuration.ProxyCreationEnabled = false;
//            ApprovalDelegateCreateFormDto model = new ApprovalDelegateCreateFormDto();
//            model.Employees = this.GetAll<Person>().OrderBy(x => x.FullName).ToList();
//            model.Apps = this.GetAll<App>().OrderBy(x => x.Name).ToList();
//            return model;
//        }

//        //http://vincentlauzon.wordpress.com/tag/entity-framework/
//        //http://vincentlauzon.wordpress.com/2011/04/11/entity-framework-4-1-deep-fetch-vs-lazy-load-3/
//        //http://vincentlauzon.wordpress.com/2011/04/21/entity-framework-4-1-bypassing-ef-query-mapping-8/
//        //http://stackoverflow.com/questions/5526422/custom-sql-functions-and-code-first-ef-4-1
//        //http://msdn.microsoft.com/en-us/library/gg679457(v=vs.103).aspx
//        private List<ApprovalDelegateDto> GetUnExpiredDelegates(int personId)
//        {
//            DateTime dt = DateTime.Today.Date;
//            //commented, but kept for understanding.
//            //ProjectionExpression<ApprovalDelegate> expression = this.ApprovalDelegates
//            //    .Include("Position")
//            //    .Include("Person")
//            //    .Where(x => x.Position.OrganizationalAssignments.Any(y => y.PersonId == personId) && (x.EndDate >= dt))
//            //    .OrderBy(x => x.StartDate)
//            //    .Project();

//            //    IQueryable<ApprovalDelegateDto> query = expression.To<ApprovalDelegateDto>();
//            //    List<ApprovalDelegateDto> list = query
//            //    .ToList();
//            //return list;
//            //TODO: should be determined: usage of Lazyloading and proxycreation
//            //these are not required for restful and especially for GET.
//            //proxycreation: (if statefulness and concurrency is a requirement and will be controlled by EF or database)
//            //based on business requirement.
//            this.Configuration.LazyLoadingEnabled = false;
//            this.Configuration.ProxyCreationEnabled = false;
//            return this.ApprovalDelegates
//                .Include("Position")
//                .Include("Person")
//                .Include("Apps")
//                .Where(x => x.Position.OrganizationalAssignments.Any(y => y.PersonId == personId) && (x.EndDate >= dt))
//                .OrderBy(x => x.StartDate)
//                .Project()
//                .To<ApprovalDelegateDto>()
//                .ToList();
//        }

//        /// <summary>
//        /// Get list of all delegates that will become active in the future
//        /// </summary>
//        /// <returns>list of all delegates that will become active in the future</returns>
//        private List<ApprovalDelegate> GetFutureDelegates()
//        {

//            var delegateResult = this.Get<ApprovalDelegate>(s => s.EndDate.Date >= DateTime.Today.Date);

//            return delegateResult.ToList<ApprovalDelegate>();

//        }

//        /// <summary>
//        /// Get all delegates with given position
//        /// </summary>
//        /// <param name="positionId">position to filter by</param>
//        /// <returns>List of all delegates with given position</returns>
//        private List<ApprovalDelegate> GetPositionsDelegates(int positionId)
//        {

//            var delegateResult = this.Get<ApprovalDelegate>(s => s.DelegatedPositionId == positionId);

//            return delegateResult.ToList<ApprovalDelegate>();

//        }

//        /// <summary>
//        /// Get all delegates with given person
//        /// </summary>
//        /// <param name="personId">person to filter by</param>
//        /// <returns>List of all delegates with given person</returns>
//        private List<ApprovalDelegate> GetPersonDelegates(int personId)
//        {
//            var delegateResult = this.Get<ApprovalDelegate>(s => s.DelegatedToPersonId == personId);

//            return delegateResult.ToList<ApprovalDelegate>();
//        }


        #endregion
    }
}
