﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Web.Helpers;

using HrAdmin2.Models;

namespace HrAdmin2.Controllers.Api
{
    public class PositionDutyController : BaseApiController
    {
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                // the ridiculously verbose way of deleting by ID from EF without pre-query.
                var pd = new PositionDuty
                {
                    Id = id
                };
                db.PositionDuties.Attach(pd);
                db.PositionDuties.Remove(pd);
                var deleted = db.SaveChanges();
                if (deleted != 1)
                {
                    throw new Exception("delete PositionDuty failed.");
                }
                else { hrm.StatusCode = HttpStatusCode.OK; }
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Post(PositionDuty pd)
        {
            try
            {
                // hack since EF doesn't support default next value sequences:
                // sequence returns Int64, not Int32 (should be safe to cast for a while)
                pd.Id = (int)db.Database.SqlQuery<Int64>("select next value for dbo.PositionDuty_seq").FirstOrDefault();
                db.PositionDuties.Add(pd);
                // this should set new ID on pd:
                int saved = db.SaveChanges();
                hrm.Content = new StringContent(Json.Encode(pd));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                        new
                        {
                            error = uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                        }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.InnerException.GetType().ToString() + ":" + (xcp.InnerException.InnerException == null ? "" : xcp.InnerException.InnerException.Message)
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Put(PositionDuty pd)
        {
            var hrm = new HttpResponseMessage();
            try
            {
                db.PositionDuties.Attach(pd);
                var e = db.Entry<PositionDuty>(pd);
                e.State = EntityState.Modified;
                var updated = db.SaveChanges();
                if (updated < 1)
                {
                    throw new Exception("PositionDuty update failed...");
                }
            }
            catch (Exception xcp)
            {
                logger.Error("PositionDuty update failed: " + xcp.Message);
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}
