﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System.Web.Helpers;

namespace HrAdmin2.Controllers.Api
{
    public class PositionDescriptionController : BaseApiController
    {
        public PositionDescriptionVm Get(int id)
        {
            var pd = db.PositionDescriptions.Find(id);
            var pdvm = new PositionDescriptionVm
            {
                CurrentUser = ui,
                Description = pd,
                Duties = db.GetByProc<PositionDuty>("GetPositionDuties", new { PositionDescriptionId = id })
            };
            return pdvm;
        }

        // TODO: move generic DeleteById function into base class...
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                // the ridiculously verbose way of deleting by ID from EF without pre-query.
                var pd = new PositionDescription
                    {
                        Id = id
                    };
                db.PositionDescriptions.Attach(pd);
                db.PositionDescriptions.Remove(pd);
                var deleted = db.SaveChanges();
//                db.Exec("DeleteCostCenter", new { Id = id });
                if (deleted != 1)
                {
                    throw new Exception("delete PositionDescription failed.");
                }
                else { hrm.StatusCode = HttpStatusCode.OK; }
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        [ActionName("Clone")]
        public HttpResponseMessage PostClone(int id)
        {
            // TODO: general utility for trying procs and returning results...
            try
            {
                var clone = db.GetSingleByProc<PositionDescriptionRow>("ClonePdf", new { PdfId = id, LanId = ui.LanId });
                hrm.Content = new StringContent(Json.Encode(clone));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.InnerException.GetType().ToString() + ":" + xcp.InnerException.InnerException.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}