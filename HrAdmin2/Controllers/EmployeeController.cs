﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class EmployeeController : BaseMvcController
    {
        public ActionResult Detail(int id)
        {
            if (id == 999999)
            {
//                logger.Error("Testing NLog Error logging");
                throw new Exception("invalid EmployeeId (MVC exception handler test)");
            }

            // this gives HR, Admin roles agency-wide span:
            var ac = db.GetSingleByProc<AccessCheck>("EmpIsInSpanOfControl", new { EmployeeId = id, LanId = ui.LanId });
            if (! ac.IsSub)
            {
                return View("AuthError");
            }

            var ed = db.GetSingleByProc<EmployeeDetail>("GetEmployeeDetail", parameters: new { EmpId = id });
            
            ed.UserInfo = ui;

            ed.Address = db.GetByProc<EmployeeAddress>("GetEmpHomeAddress",
                  parameters: new { EmpId = id });

            ed.Leave = db.GetByProc<EmployeeLeave>("GetLeave",
                 parameters: new { EmpId = id });

            ed.EmerContacts = db.GetByProc<EmergencyContacts>("GetEmpEmerContacts",
                  parameters: new { EmpId = id });
            var evm = new EmployeeVm()
            {
                CurrentUser = ui,
                Emp = ed,
                ApptHistory = db.GetByProc<Appointment>("GetEmployeeAppts", parameters: new { EmpId = id })
            };
//            return View(ed);
            return View(evm);
        }
    }

    public class AccessCheck
    {
        public bool IsSub { get; set; }
    }
}