﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class ParController : BaseMvcController
    {
        // GET: /Par/
        public ActionResult Index(ParQuery q)
        {
            var cc = ControllerContext;
            if (cc.HttpContext.Request.AppRelativeCurrentExecutionFilePath.Length == 2) // redirect root-level requests ("~/") to Par controller explicitly (for bookmarking purposes):
            {
                return new RedirectResult(Url.Content("~/Par?WorkflowStatusId=1")); // redirect to CTS root OrgUnit if unspecified
            }
            //return new RedirectResult(Url.Content("~/par/")); // redirect to CTS root OrgUnit if unspecified
            //var pl = from p in db.GetByProc<Par>("GetParList", parameters: new { })
            //         orderby p.Id
            //         select p;
            // ordered by EffectiveDate:

            // TODO replace all null string props with empty strings...
            if (q.ActionCode == null) { q.ActionCode = ""; }

//            if (!q.FromDate.HasValue)
//            {
//                q.FromDate = null;
////                q.FromDate = DateTime.Now.AddMonths(-3); // default from 3 months ago
//            }
            if (String.IsNullOrEmpty(q.Query))
            {
                q.Query = "";
            }
            //if (!q.ToDate.HasValue)
            //{
            //    q.ToDate = DateTime.Now;
            //}
//            q.LanId = User.Identity.Name;
            q.LanId = ui.LanId;
            var pl = db.GetByProc<Par>("GetParList", parameters: q);
            var vm = new ParListVm
            {
                Query = q,
                Results = pl,
                ActionTypes = db.GetByProc<SelectListItem>("GetParActionTypes", parameters: new {}),
//                ParStatus = db.GetByProc<SelectListItem>("GetParStatusList", parameters: new {})
                ApprovalStatuses = db.GetByProc<SelectListItem>("GetApprovalStatusSelection", parameters: new {}),
//                OrgUnit = db.GetByProc<SelectListItem>("GetOuSelection", parameters: new { })
                CurrentUser = ui
            };
            PrependBlankSelection(vm.ActionTypes);
            PrependBlankSelection(vm.ApprovalStatuses);
            //PrependBlankSelection(vm.OrgUnit);
//            return View(vm);
            return View("ParNg", vm);
        }

        public ActionResult Detail(int id)
        {
            // now passing LanId, requiring PAR position to be in user's span of control unless HR or Admin role.
//            var parDetail = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id });
            try
            {
//                var parDetail = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id, LanId = User.Identity.Name });
                var parDetail = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id, LanId = ui.LanId });
                if (parDetail.ContractTypeCode != null && parDetail.ContractTypeCode.Equals("  "))
                {
                    // blanks break Angular binding.
                    parDetail.ContractTypeCode = "--";
                }
                var vm = new ParDetailVm(parDetail, db)
                {
                    CanDelete = new AuthFilter.BaseAuthorizationProvider(ControllerContext.HttpContext).Can(ModelAction.Delete),
                    CurrentUser = ui
                };
                vm.ApprovalWorkflowVm.SetAuthorization(ui.LanId);//set authorization for workflow tasks
                //return View(vm); // Views\Par\Detail.cshtml (implicit)
                // change View to DetailNg to run AngularJS version
                return View("~/Views/Par/DetailNg.cshtml", vm);
            }
            catch (Exception xcp)
            {
                // could return AuthError if actual auth exception...
                // TODO: log Exception detail...
                return View("Error");
            }
        }

        public ActionResult EmailPreview(int id)
        {
            var parDetail = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id, LanId = ui.LanId });
            var vm = new ParDetailVm(parDetail, db);
            return View("EmailTemplate", vm); // Views\Par\EmailTemplate.cshtml (explicit)
        }

        public class ParQuery
        {
            public string ActionCode { get; set; }
            public int OrgUnitId { get; set; }
            public int ParStatusId { get; set; }
            public int WorkflowStatusId { get; set; }
            public int EmployeeId { get; set; }
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
            public string Query { get; set; }
            public string LanId { get; set; }
        }

        public class ParListVm : BaseVm
        {
            public IEnumerable<Par> Results { get; set; }
            public ParQuery Query { get; set; }
            public List<SelectListItem> ActionTypes { get; set; }
//            public List<SelectListItem> OrgUnit { get; set; }
//            public List<SelectListItem> ParStatus { get; set; }
            public List<SelectListItem> ApprovalStatuses { get; set; }
        }

        // TODO: move this to Util class.  duplicated in ParSelector model...
        public static void PrependBlankSelection(List<SelectListItem> optlist)
        {
            optlist.Insert(0, new SelectListItem()
            {
                Text = " - all - ", // this is different (normally "select")
                Value = "",
                Selected = true
            });
        }

    }
}
