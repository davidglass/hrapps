﻿using AuthFilter;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class DelegationController : BaseMvcController
    {
        private HrAdminDbContext db = new HrAdminDbContext();


        public ActionResult Index()
        {
            var dvm = new DelegationListVm(db) { CurrentUser = ui };
            //return View(dvm);
            return View("~/Views/Delegation/IndexNg.cshtml", dvm);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
