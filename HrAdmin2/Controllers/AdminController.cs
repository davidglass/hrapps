﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class AdminController : BaseMvcController
    {
        //private HrAdminDbContext db = new HrAdminDbContext();
        //
        // GET: /Admin/
        public ActionResult Index()
        {
//            return View(new AdminVm(db));
            return View(new AdminVm() { CurrentUser = ui });
        }

        public ActionResult EmailPreviewExpiringContract(int empid)
        {
            var contractinfo = db.GetSingleByProc<EmpContractEmailInfo>("GetEmpContractEmailInfo", new { EmpId = empid });
            return View("EmailExpiringContract", contractinfo); // Views\Par\EmailTemplate.cshtml (explicit)
        }
    }
}
