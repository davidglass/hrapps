﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    public class NgPartialController : BaseMvcController
    {
        //
        // GET: /NgPartial/PartialName
        public ActionResult Template(string pview)
        {
            return PartialView(String.Format("~/Views/NgPartial/{0}.cshtml", pview), GetViewModelForPartial(pview));
        }

        private BaseVm GetViewModelForPartial(string pview)
        {
            switch (pview)
            {
                case "PdfDetail": return new PdfDetailVm
                    {
                        CurrentUser = ui,
                        JobClasses = db.GetByProc<SelectListItem>("GetJobClassSelection", new {})
                    };
                default:
                    return new BaseVm
                    {
                        CurrentUser = ui
                    };
            }
        }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    // *NOTE*, this is only for providing server-generated form components (e.g. drop-down lists, etc.):
    public class PdfDetailVm : BaseVm
    {
        public List<SelectListItem> JobClasses { get; set; }
    }
}