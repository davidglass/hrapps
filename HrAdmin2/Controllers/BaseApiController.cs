﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

using HrAdmin2.Models;
using System.Web.Http.Controllers;

namespace HrAdmin2.Controllers
{
    public class BaseApiController : ApiController
    {
        protected HrAdminDbContext db = new HrAdminDbContext();
        protected CurrentUserInfo ui;
        protected HttpResponseMessage hrm = new HttpResponseMessage();
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        // http://msdn.microsoft.com/en-us/library/system.web.mvc.controller.initialize(v=vs.118).aspx
        protected override void Initialize(HttpControllerContext cc)
        {
            ui = db.GetSingleByProc<CurrentUserInfo>("GetUserInfoNew", new { LanId = User.Identity.Name });
            base.Initialize(cc);
        }
    }
}