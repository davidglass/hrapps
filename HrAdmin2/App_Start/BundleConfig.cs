﻿using System.Web;
using System.Web.Optimization;

namespace HrAdmin2
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/jquery.maskMoney_min.js",
                        "~/Scripts/jquery.maskedinput_min.js"));
//                        "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-1.10.3.min.js"));
            // renamed minified file since default config ignores them!!!
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.10.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));
            // wildcard version match grabs debug version!
            //bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
            //"~/Scripts/knockout-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
//            "~/Scripts/knockout-2.2.0.js",
            "~/Scripts/knockout-2.2.1.js",
            "~/Scripts/knockout.mapping-latest.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/custom/css").Include(
                        "~/Content/themes/custom/jquery.ui.core.css",
                        "~/Content/themes/custom/jquery.ui.resizable.css",
                        "~/Content/themes/custom/jquery.ui.selectable.css",
                        "~/Content/themes/custom/jquery.ui.accordion.css",
                        "~/Content/themes/custom/jquery.ui.autocomplete.css",
                        "~/Content/themes/custom/jquery.ui.button.css",
                        "~/Content/themes/custom/jquery.ui.dialog.css",
                        "~/Content/themes/custom/jquery.ui.slider.css",
                        "~/Content/themes/custom/jquery.ui.tabs.css",
                        "~/Content/themes/custom/jquery.ui.datepicker.css",
                        "~/Content/themes/custom/jquery.ui.progressbar.css",
                        "~/Content/themes/custom/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/slickGrid").Include(
                        //"~/Scripts/jquery.event.drag-2.2.js",
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.event.drag.js",
                        "~/Scripts/SlickGrid/slick.core.js",
                        "~/Scripts/SlickGrid/slick.grid.js",
                        "~/Scripts/SlickGrid/slick.dataview.js",
                        "~/Scripts/SlickGrid/Plugins/slick.rowselectionmodel.js"));

            bundles.Add(new StyleBundle("~/Content/slickGrid").Include(
                        "~/Content/SlickGrid/slick.grid.css"));
        }
    }
}