﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HrAdmin2
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // global API Exception filter:
            // http://www.asp.net/web-api/overview/web-api-routing-and-actions/exception-handling#registering_exception_filters
            config.Filters.Add(new Filters.LoggedExceptionFilterAttribute());

            //config.Routes.MapHttpRoute(
            //    name: "PositionIncumbents",
            //    routeTemplate: "api/position/{posid}/incumbent",
            //    defaults: new
            //    {
            //        action = "GetIncumbents",
            //        controller = "Position"
            //    }
            //);

            // controller here is always Par, but could be other controllers with attachments:
            config.Routes.MapHttpRoute(
                name: "Attachments",
                routeTemplate: "api/{controller}/{id}/attachment",
                defaults: new
                {
                    action = "Attachment" // adds attachment to referenced item
                }
            );

            config.Routes.MapHttpRoute(
                name: "Clone",
                routeTemplate: "api/{controller}/{id}/clone",
                defaults: new
                {
                    action = "Clone" // clones referenced item, returns clone (with new ID) in reponse...
                }
            );

            config.Routes.MapHttpRoute(
                name: "Email",
                routeTemplate: "api/{controller}/{id}/email",
                defaults: new
                {
                    action = "Email" // creates and/or sends Email related to referenced item
                }
            );

            config.Routes.MapHttpRoute(
                name: "ParEmp",
                routeTemplate: "api/{controller}/{id}/emp/{empid}",
                defaults: new
                {
                    action = "ParEmp"
                }
            );

            config.Routes.MapHttpRoute(
                name: "Par",
                routeTemplate: "api/{controller}/{id}/par",
                defaults: new
                {
                    action = "Par"
                }
            );

           
            // controller here is always OrgUnit, but could be other controllers with hierarchical models:
            config.Routes.MapHttpRoute(
                name: "SubNodes",
                routeTemplate: "api/{controller}/{id}/subnodes",
                defaults: new
                {
                    action = "GetLazyExpand"
                }
            );

            config.Routes.MapHttpRoute(
                name: "SubNodesAll",
                routeTemplate: "api/{controller}/{id}/subnodes/all",
                defaults: new
                {
                    action = "GetLazyExpandAll"
                }
            );

            // this is to differentiate OuDetail get from 
            config.Routes.MapHttpRoute(
                name: "OuDetail",
                routeTemplate: "api/{controller}/{id}/detail",
                defaults: new
                {
                    action = "Get"
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}", // currently allowing any string for id.  multi-update / delete can use CSV..
                defaults: new { id = RouteParameter.Optional }
            );

            // this code for defaulting to JSON courtesy Glenn Slaven:
            // http://stackoverflow.com/questions/9847564/how-do-i-get-mvc-4-webapi-to-return-json-instead-of-xml-using-chrome
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
}