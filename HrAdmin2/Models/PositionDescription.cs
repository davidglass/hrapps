﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
//    public class PositionDescription : ModelBase
    // removing ModelBase to remove Validate / LastUpdatedByUser properties...
    // TODO: move them out of ModelBase.
    public class PositionDescription
    {
        public int Id { get; set; }
        public int PositionId { get; set; }
        public string PositionObjective { get; set; }
        public int ProposedJobId { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
    }

    public class PositionDescriptionRow
    {
        public int Id { get; set; }
        public int ProposedJobId { get; set; }
        public string ProposedClassCode { get; set; }
        public string ProposedClassTitle { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
    }

//    public class PositionDuty : ModelBase
    public class PositionDuty
    {
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int Id { get; set; } // sequence returns long, being cast to int.
        public int PositionDescriptionId { get; set; }
        public string Duty { get; set; }
        public string TasksHtml { get; set; }
        public int TimePercent { get; set; }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class PositionDescriptionVm : BaseVm
    {
        public PositionDescription Description { get; set; }
        public List<PositionDuty> Duties { get; set; }
    }
}