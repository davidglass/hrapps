﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HrAdmin2.Models
{
    // no longer subclassing ModelBase due to conflict with Validate property and EF...
//    public class ParEmail: ModelBase
    public class ParEmail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 
        public int ParId { get; set; }
        public string HtmlBody { get; set; }
        public DateTime TimeSent { get; set; }
        public string SentTo { get; set; }
        public string SentBcc { get; set; }
        public string SentFrom { get; set; }
        // AttachmentIds: comma-separated List<int>
        public string AttachmentIds { get; set; }
    }
}