﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class Attachment
    {

        public int AttachmentId { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public long SizeInBytes { get; set; }
        public string MimeType { get; set; }
     
        public byte[] Content { get; set; }

        public Attachment() { }

    }
}