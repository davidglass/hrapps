﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using HrAdmin2.Models.Validators;

namespace HrAdmin2.Models
{
    // TODO: class-level custom validator
    public class ParDetail : Par
    {
        // TODO: create custom validators, 
        // TODO (?): move employee info into EmployeeInfo model / property
        // , position info into PositionInfo
        // , appointment info into AppointmentInfo

        // EmployeeInfo
        // moved EmployeeId to base class
        //public int EmployeeId { get; set; }
        [CustomValidation(typeof(EmpHrmsIdValidator), "Validate")]
        public int EmpHrmsId { get; set; } // default 0 for New Hires, > 0 for others...
        // First and Last are in base class for use in summary lists...
        //public string EmpFirstName { get; set; }
        public bool? EmpFirstNameChanged { get; set; }
        //public string EmpLastName { get; set; }
        public bool? EmpLastNameChanged { get; set; }
        public string EmpMiddleName { get; set; }
        public bool? EmpMiddleNameChanged { get; set; }
        public string EmpPreferredName { get; set; }
        public bool? EmpPreferredNameChanged { get; set; }
        public string EmpSuffix { get; set; }
        public bool? EmpSuffixChanged { get; set; }
        public string EmpEmail { get; set; }
        //[CustomValidation(typeof(NewHireSSN), "Validate")]
        public string EmpSSN { get; set; } // should be blanked after PrNr received from HRMS. (or better, store only in PAR, blank there)
        //[CustomValidation(typeof(NewHireRequiredNullableDate), "Validate")]
        public DateTime? EmpDOB { get; set; }
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrStreetLine1 { get; set; } // varchar(60)
        public string EmpAddrStreetLine2 { get; set; } // varchar(40)
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrCity { get; set; }
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrCountyCode { get; set; }
        public string EmpAddrCountyName { get; set; }
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrState { get; set; }
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrZipCode { get; set; }
        [CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpAddrHomePhone { get; set; }
        public DateTime? EmpDS01_Anniversary { get; set; }
        public bool? EmpDS01_AnniversaryChanged { get; set; }
        [CustomValidation(typeof(ApptRequiredNullable), "Validate")]
        public DateTime? EmpDS02_Appointment { get; set; }
        public bool? EmpDS02_AppointmentChanged { get; set; }
        [CustomValidation(typeof(ApptRequiredNullable), "Validate")]
        public DateTime? EmpDS03_CtsHire { get; set; }
        public bool? EmpDS03_CtsHireChanged { get; set; }
        public DateTime? EmpDS04_PriorPid { get; set; }
        public bool? EmpDS04_PriorPidChanged { get; set; }
        public DateTime? EmpDS05_Seniority { get; set; }
        public bool? EmpDS05_SeniorityChanged { get; set; }
        public DateTime? EmpDS07_UnbrokenService { get; set; }
        public bool? EmpDS07_UnbrokenServiceChanged { get; set; }
        public DateTime? EmpDS09_VacLeaveFrozen { get; set; }
        public bool? EmpDS09_VacLeaveFrozenChanged { get; set; }
        public DateTime? EmpDS18_PersonalHolidayElg { get; set; }
        public bool? EmpDS18_PersonalHolidayElgChanged { get; set; }
        public DateTime? EmpDS26_PersonalLeaveElg { get; set; }
        public bool? EmpDS26_PersonalLeaveElgChanged { get; set; }
        // no more TSR...
        //public DateTime EmpDS27_TsrLeaveElg { get; set; }
        //[CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpRaceCodes { get; set; }
        //[CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpGender { get; set; } // 1=male, 2=female... SQL char converts to string.
        public bool? EmpHispanic { get; set; }
        //[CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpMilitaryStatus { get; set; }
        //[CustomValidation(typeof(NewHireRequiredString), "Validate")]
        public string EmpMilitaryBranch { get; set; }
        public bool? EmpDisability { get; set; }

        // PositionInfo
        [CustomValidation(typeof(PosRequiredIntValidator), "Validate")]
        public int? PosCostCenter { get; set; }
        public bool? PosCostCenterChanged { get; set; }
        [CustomValidation(typeof(RequiredString), "Validate")]
        public string PosEeGroup { get; set; }
        public bool? PosEeGroupChanged { get; set; }
        [CustomValidation(typeof(RequiredString), "Validate")]
        public string PosEeSubgroup { get; set; }
        public bool? PosEeSubgroupChanged { get; set; }
        public string PosGeneralDescription { get; set; }
        public bool? PosGeneralDescriptionChanged { get; set; }

        public int PosHrmsId { get; set; }
        // PosOrgUnitId is in base class:
        public int? PosHrmsOrgUnitId { get; set; }
        public int PosHrmsSupervisorId { get; set; }
        public bool PosIsInTraining { get; set; }
        public bool? PosIsInTrainingChanged { get; set; }
        public bool PosIsAppointingAuthority { get; set; } // Appointing Authority
        public bool? PosIsAppointingAuthorityChanged { get; set; }
        public bool PosIsManager { get; set; }
        public bool? PosIsManagerChanged { get; set; }
        public bool PosIsSection4 { get; set; }
        public bool? PosIsSection4Changed { get; set; }
        public bool PosIsSupervisor { get; set; }
        public bool? PosIsSupervisorChanged { get; set; }
        public bool PosIsTandem { get; set; }
        public bool? PosIsTandemChanged { get; set; }
        public string PositionClassCode { get; set; }
        public string PositionJobTitle { get; set; }
        [CustomValidation(typeof(PosRequiredIntValidator), "Validate")]
        public int? PosJobId { get; set; }
        public bool? PosJobIdChanged { get; set; }
        public string PosJvac { get; set; }
        public bool? PosJvacChanged { get; set; }
        public string PosLocation { get; set; }
        [CustomValidation(typeof(PosRequiredIntValidator), "Validate")]
        public int? PosLocationId { get; set; }
        public bool? PosLocationIdChanged { get; set; }
        // moved to base class:
        //public int PosOrgUnitId { get; set; } // CTS OuId (normally same as Hrms)
        public bool? PosOrgUnitIdChanged { get; set; }
        [CustomValidation(typeof(RequiredString), "Validate")]
        public string PosPersonnelSubArea { get; set; }
        public bool? PosPersonnelSubAreaChanged { get; set; }
        public int? PosPointValue { get; set; }
        public bool? PosPointValueChanged { get; set; }

        public string PosPayGradeArea { get; set; }
        public string PosPayGradeAreaDesc { get; set; }
        public string PosPayGradeType { get; set; }
        public string PosPayGradeTypeDesc { get; set; }

        [CustomValidation(typeof(PosRequiredIntValidator), "Validate")]
        public int? PosPayGradeTypeArea { get; set; }
        public bool? PosPayGradeTypeAreaChanged { get; set; }
        public string PosPayGradeTypeAreaDesc { get; set; }
        [CustomValidation(typeof(RequiredString), "Validate")]
        public string PosPayGrade { get; set; }
        public bool? PosPayGradeChanged { get; set; }

        public bool PosReqsBackgroundCheck { get; set; }
        public bool? PosReqsBackgroundCheckChanged { get; set; }
        public string PosShiftCode { get; set; }
        public bool? PosShiftCodeChanged { get; set; }
        public bool PosShiftDiffEligible { get; set; }
        public bool? PosShiftDiffEligibleChanged { get; set; }
        public bool PosStandbyEligible { get; set; }
        public bool? PosStandbyEligibleChanged { get; set; }
        //public string PosShiftDesignation { get; set; }
        //public bool? PosShiftDesignationChanged { get; set; }
        // moved PosSupervisorPosId to base class:
        // PosSupervisorPosId is null for Director:
        //public int? PosSupervisorPosId { get; set; } // CTS Pos Id
        //public bool? PosSupervisorPosIdChanged { get; set; }
        [CustomValidation(typeof(RequiredString), "Validate")]
        public string PosWorkersCompCode { get; set; }
        public bool? PosWorkersCompCodeChanged { get; set; }
        public string WorkersCompDescription { get; set; }
        public string PosWorkingTitle { get; set; }
        public bool? PosWorkingTitleChanged { get; set; }
        public string PosWorkScheduleCode { get; set; }
        public bool? PosWorkScheduleCodeChanged { get; set; }
        public string PosWorkScheduleDescription { get; set; }

        // AppointmentInfo
        // TODO: fix problem with nullable Date property:
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ApptFillOtEligCode { get; set; }
        public bool? ApptFillOtEligCodeChanged { get; set; }

        [CustomValidation(typeof(ApptRequiredNullable), "Validate")]
        public DateTime? ApptPayScaleEffectiveDate { get; set; }
        public bool? ApptPayScaleEffectiveDateChanged { get; set; }

        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ApptPayScaleReason { get; set; } // SQL char(2)
        public bool? ApptPayScaleReasonChanged { get; set; }
        public string ApptPayScaleReasonDesc { get; set; }
        [CustomValidation(typeof(ApptRequiredIntValidator), "Validate")]
        public int? ApptPayScaleTypeArea { get; set; }
        public bool? ApptPayScaleTypeAreaChanged { get; set; }
        public string ApptPayScaleTypeAreaDesc { get; set; }
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ApptPayScaleGroup { get; set; }
        public bool? ApptPayScaleGroupChanged { get; set; }
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ApptPayScaleLevel { get; set; }
        public bool? ApptPayScaleLevelChanged { get; set; }

        [CustomValidation(typeof(ApptDecimalNumRange), "ValidateSalary")]
        public decimal ApptPayAnnualSalary { get; set; }
        public bool? ApptPayAnnualSalaryChanged { get; set; }
        public decimal ApptPayCapUtilityLevel { get; set; }
        public decimal? ApptPayHourlyRate { get; set; }
        public bool? ApptPayHourlyRateChanged { get; set; }
        public DateTime? ApptPayNextIncrease { get; set; }
        public bool? ApptPayNextIncreaseChanged { get; set; }
        public bool ApptShiftDifferential { get; set; }
        public bool? ApptShiftDifferentialChanged { get; set; }
        public bool ApptStandbyPay { get; set; }
        public bool? ApptStandbyPayChanged { get; set; }
        public DateTime? ApptStepMEligibleDate { get; set; }
        public bool? ApptStepMEligibleDateChanged { get; set; }
        // TODO: move this to CustomValidator.  Is not required for non-Appointment (e.g. Position) PARs.
        //[Required]
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ApptTimeMgtStatus { get; set; } // SQL char(1), default='9'
        public bool? ApptTimeMgtStatusChanged { get; set; }
        public string ContractType { get; set; }
        //public string ApptShiftDesignation { get; set; }
        public string ApptWorkHoursWk1 { get; set; }
        public string ApptWorkHoursWk2 { get; set; }

        // hack to allow "  " (two blank spaces) as legitimate required value:
        // Permanent Contract Code ("  ", two spaces) fails Required validation without this.
        // TODO: move this to CustomValidator.  ContractTypeCode is not required for non-Appointment (e.g. Position) PARs.
        ////[Required(AllowEmptyStrings = true)]
        //[MinLength(2, ErrorMessage = "Contract Type is required")]
        [DisplayFormat(ConvertEmptyStringToNull=false)]
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string ContractTypeCode { get; set; }
        public bool? ContractTypeCodeChanged { get; set; }

        public DateTime? ContractEndDate { get; set; }
        public bool? ContractEndDateChanged { get; set; }
        // TODO: move this to CustomValidator.  Is not required for non-Appointment (e.g. Position) PARs.
        //[Range(1, 100)]
        [CustomValidation(typeof(ApptDecimalNumRange), "Validate")]
        public decimal PercentFullTime { get; set; }
        public bool? PercentFullTimeChanged { get; set; }
        [CustomValidation(typeof(ApptRequiredIntValidator), "Validate")]
        public int? FillLevel { get; set; }
        public bool? FillLevelChanged { get; set; }
        public string FillClass { get; set; }
        public string FillTitle { get; set; }
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string WorkScheduleCode { get; set; }
        public bool? WorkScheduleCodeChanged { get; set; }
        public string WorkScheduleDescription { get; set; }

        // Par detail omitted from base class
        //public string ActionCode { get; set; } // char(2) or "UNSPECIFIED"
        public string ActionReasonCode { get; set; }
        // moved to base class:
//        public int? ActionReasonId { get; set; }
        public string Notes { get; set; }
        public DateTime? ParEmailSentAt { get; set; }
        public List<ParAttachment> Attachments { get; set; }
        public int? StatusId { get; set; }
        [CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string TargetAgencyCode { get; set; } // AKA Personnel Area Code
        //[CustomValidation(typeof(CodeStringValidator), "Validate")]
        public string FromAgencyCode { get; set; } // AKA Personnel Area Code
        public string AgencyDescription { get; set; }
        // moved to base class:
        //public bool IsTransferOut { get; set; }
        public string UpdateAction { get; set; }
        public bool DeactivateClassCode { get; set; }
        public int? ApprovalStatusId { get; set; }
        public bool HasCheckList { get; set; }
    }
}