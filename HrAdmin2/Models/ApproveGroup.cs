﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ApproveGroup
    {
        public ApproveGroup()
        {
            this.ApproveGroupPositions = new List<Position>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public List<Position> ApproveGroupPositions { get; set; }
    }
}