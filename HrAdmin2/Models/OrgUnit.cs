﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class OrgUnit : ModelBase
    {
        public int? HrmsOrgUnitId { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public string Abbreviation { get; set; }
        public int? SupervisorPosId { get; set; }
    }
}