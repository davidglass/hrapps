﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using HrAdmin2.Models.Validators;

namespace HrAdmin2.Models
{
    public class ApprovalDelegate
    {
        public ApprovalDelegate()
        {
            this.Apps = new List<ApprovalApp>();
        }

        public int Id { get; set; }

        [CustomValidation(typeof(DelegateIdValidator), "Validate")]
        public int DelegatedToPersonId { get; set; }

        [CustomValidation(typeof(DelegateIdValidator), "Validate")]
        public int DelegatedPositionId { get; set; }

        [CustomValidation(typeof(DelegateRequiredDate), "Validate")]
        public System.DateTime StartDate { get; set; }

        public System.DateTime EndDate { get; set; }

        public bool Indefinite { get; set; }

        public List<ApprovalApp> Apps { get; set; }
    }
}