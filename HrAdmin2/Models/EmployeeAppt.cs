﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class EmployeeAppt : ModelBase
    {
        // (corresponds to a row from GetIncumbentAppts sproc)
        public int EmpId { get; set; }
        public int FillLevel { get; set; }
        public string FillLevelDesc { get; set; }
        public string ContractTypeCode { get; set; }
        public string WorkContractTypeDesc { get; set; }
        public string WorkScheduleCode { get; set; }
        public string WorkScheduleDesc { get; set; }
        public Decimal EmploymentPercent { get; set; }
        public int? PersonnelNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public string Suffix { get; set; }
        public string Gender { get; set; } // char(1) in db.
        public string MaritalStatus { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StreetLine1 { get; set; }
        public string CountyId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string HomePhone { get; set; }
        public DateTime? AnniversaryDate { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public DateTime? CtsHireDate { get; set; }
        public DateTime? PriorPidDate { get; set; }
        public DateTime? SeniorityDate { get; set; }
        public DateTime? UnbrokenServiceDate { get; set; }
        public DateTime? VacLeaveFrozenDate { get; set; }
        public DateTime? PersonalHolidayElgDate { get; set; }
        public DateTime? PersonalLeaveElgDate { get; set; }
        public DateTime? TsrLeaveElgDate { get; set; }
    }
}