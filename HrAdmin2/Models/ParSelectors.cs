﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrAdmin2.Models
{
    public class ParSelectors
    {
        private HrAdminDbContext db;
        public ParSelectors(HrAdminDbContext hrdb)
        {
            db = hrdb;
            Init();
        }

        private void Init()
        {
            // populate selector lists...
            // changed this to return "--" for Permanent ContractTypeCode.  Blank spaces break Angular binding.
            Contract = db.GetByProc<SelectListItem>("GetContractSelection2", parameters: new { });
            // 2013-08-09: GetCostCenterSelection proc still must be written -DG
            CostCenter = db.GetByProc<SelectListItem>("GetCostCenterSelection", parameters: new { });
            County = db.GetByProc<SelectListItem>("GetCountySelectList", parameters: new { });
            CurrentEmp = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", parameters: new { });
            EmpGroup = db.GetByProc<SelectListItem>("GetEmployeeGroups", parameters: new { });
            EmpSubGroup = db.GetByProc<SelectListItem>("GetEmployeeSubGroups", parameters: new { });
            JobClass = db.GetByProc<SelectListItem>("GetJobClassSelection", parameters: new { });
            Location = db.GetByProc<SelectListItem>("GetLocationSelection", parameters: new { });
            // this could be bypassed for non fill-with-existing-emp actions:
            OrgUnit = db.GetByProc<SelectListItem>("GetOuSelection", parameters: new { });
            ParStatus = db.GetByProc<SelectListItem>("GetParStatusList", parameters: new { });
            PayScaleReason = db.GetByProc<SelectListItem>("GetPayScaleReasons", parameters: new { });
            PayScaleTypeArea = db.GetByProc<SelectListItem>("GetPayScaleTypeArea", parameters: new { });
            PayScaleGroup = db.GetByProc<SelectListItem>("GetPayScaleGroup", parameters: new { });
            PayScaleLevel = db.GetByProc<SelectListItem>("GetPayScaleLevel", parameters: new { });
            PersonnelSubArea = db.GetByProc<SelectListItem>("GetPersonnelSubAreaSelection", parameters: new { });
            Schedule = db.GetByProc<SelectListItem>("GetWorkScheduleRules", parameters: new { });
            SupervisorPosition = db.GetByProc<SelectListItem>("GetSupervisorSelection", parameters: new { });
            TargetAgency = db.GetByProc<SelectListItem>("GetAgencySelection", parameters: new { });
            WorkersComp = db.GetByProc<SelectListItem>("GetWorkersCompSelection", parameters: new { });

            // alternatively, this could be done in sprocs, but seems to belong here.
            PrependBlankSelection(Contract);
            PrependBlankSelection(CostCenter);
            PrependBlankSelection(County);
            PrependBlankSelection(CurrentEmp);
            PrependBlankSelection(EmpGroup);
            PrependBlankSelection(EmpSubGroup);
            PrependBlankSelection(JobClass);
            PrependBlankSelection(Location);
            PrependBlankSelection(OrgUnit);
            PrependBlankSelection(ParStatus);
            PrependBlankSelection(PayScaleGroup);
            PrependBlankSelection(PayScaleLevel);
            PrependBlankSelection(PayScaleReason);
            PrependBlankSelection(PayScaleTypeArea);
            PrependBlankSelection(PersonnelSubArea);
            PrependBlankSelection(Schedule);
            PrependBlankSelection(SupervisorPosition);
            PrependBlankSelection(TargetAgency);
            PrependBlankSelection(WorkersComp);
        }

        public List<SelectListItem> Contract { get; set; }
        public List<SelectListItem> CostCenter { get; set; }
        public List<SelectListItem> County { get; set; }
        public List<SelectListItem> CurrentEmp { get; set; }
        public List<SelectListItem> EmpGroup { get; set; }
        public List<SelectListItem> EmpSubGroup { get; set; }
        public List<SelectListItem> JobClass { get; set; }
        public List<SelectListItem> Location { get; set; }
        public List<SelectListItem> OrgUnit { get; set; }
        public List<SelectListItem> ParStatus { get; set; }
        public List<SelectListItem> PayScaleGroup { get; set; }
        public List<SelectListItem> PayScaleLevel { get; set; }
        public List<SelectListItem> PayScaleReason { get; set; }
        public List<SelectListItem> PayScaleTypeArea { get; set; }
        public List<SelectListItem> PersonnelSubArea { get; set; }
        public List<SelectListItem> Schedule { get; set; }
        public List<SelectListItem> TargetAgency { get; set; }
        public List<SelectListItem> SupervisorPosition { get; set; }
        public List<SelectListItem> WorkersComp { get; set; }

//        private void PrependBlankSelection(List<SelectListItem> optlist)
        public static void PrependBlankSelection(List<SelectListItem> optlist)
        {
            optlist.Insert(0, new SelectListItem()
            {
                Text = " - select - ",
                Value = "",
                Selected = true
            });
        }

    }
}