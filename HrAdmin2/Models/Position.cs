﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class Position : ModelBase
    {
        public string JobTitle { get; set; }
        public string WorkingTitle { get; set; }
        public int CostCenter { get; set; }
        public int HrmsPositionId { get; set; }
        public int OrgUnitId { get; set; }
        public string PersonnelSubArea { get; set; }
        public string LegacyClassCode { get; set; }
        public string OuDescription { get; set; } // TODO: move this to ViewModel? and/or add OrgUnit property
        public string EeGroupDescription { get; set; } // TODO: move this to ViewModel?
        public string EeSubGroupDescription { get; set; } // TODO: move this to ViewModel?
        public string ShiftCode { get; set; } // (D)ay, (S)wing, (G)rave
        public int? KeyCompId { get; set; }
        public int? PdfId { get; set; }
        public int? WmsId { get; set; }
        public string LegacyPositionNum { get; set; }
        public string WorkSchedDesc { get; set; }
        public string WorkersCompDesc { get; set; }
        public string PayGradeTypeAreaDesc { get; set; }
        public string PayGrade { get; set; }
        public bool IsAppointingAuthority { get; set; }
        public bool IsManager { get; set; }
        public bool IsSupervisor { get; set; }
        public bool IsBackgroundCheckRequired { get; set; }
        public bool IsSection4 { get; set; }
        public bool IsTandem { get; set; }
        public bool IsInTraining { get; set; }
        public string Jvac { get; set; }
        public int? JvacPoints { get; set; }
    }
}