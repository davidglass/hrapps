﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using HrAdmin2.Models.Validators;

namespace HrAdmin2.Models
{
    /// <summary>
    ///  list view version of ActionRequest...subset of fields, Appt info, etc.
    ///  
    /// </summary>
    public class Par : ModelBase
    {
        public string ActionCode { get; set; } // e.g. 'U3', 'P1', etc.
        // this is set nullable to allow nullifying reason:
        //[CustomValidation(typeof(ActionReasonIdValidator), "Validate")]
        [CustomValidation(typeof(ApptRequiredIntValidator), "Validate")]
        public int? ActionReasonId { get; set; }
        public string ActionType { get; set; } // ApptActionReason.Reason Description
        public string ActionDescription { get; set; }
        public int? EmployeeId { get; set; } // can be null when updating New Hire (ignored by update)
        public int PositionId { get; set; }
        [CustomValidation(typeof(ParRequiredString), "Validate")]
        public string EmpFirstName { get; set; }
        [CustomValidation(typeof(ParRequiredString), "Validate")]
        public string EmpLastName { get; set; }
        public string EmpLastFirst { get; set; }
//        public int OrgUnitId { get; set; }
        [CustomValidation(typeof(PosRequiredIntValidator), "Validate")]
        public int? PosOrgUnitId { get; set; }
        public string OrgUnitShortName { get; set; }
        public string Originator { get; set; }
        public string WorkflowStatusDesc { get; set; }
        public int WorkflowStatusId { get; set; }
        [CustomValidation(typeof(ParRequiredNullable), "Validate")]
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ParStatusDesc { get; set; }
        public int? ApprovalWorkflowId { get; set; }

        public int? PosSupervisorPosId { get; set; } // CTS Pos Id
        public bool? PosSupervisorPosIdChanged { get; set; }

//        public string WaitingOn { get; set; }
        public string ApprovalStatusDesc { get; set; }
        public bool IsTransferIn { get; set; }
        public bool IsTransferOut { get; set; }
        public bool IsPayChangeOnly { get; set; }
    }
}