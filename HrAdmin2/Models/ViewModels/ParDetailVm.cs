﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace HrAdmin2.Models.ViewModels
{
//    public class ParDetailVm : HrAdmin2.Models.BaseVm
    public class ParDetailVm : BaseVm
    {
        // TODO: give ParDetail properties for Employee, Appointment, Action, Position
        private ParDetail pd;
        private HrAdminDbContext db;

        public ParDetailVm(ParDetail _pd, HrAdminDbContext _db)
        {
            db = _db;
            pd = _pd;
            Init();   
        }

        private void Init()
        {
            pd.Attachments = db.GetByProc<ParAttachment>("GetParAttachments", new { ParId = pd.Id });
            Selectors = new ParSelectors(db);
            ActionReasons = pd.ActionCode.IndexOf('U') == 0 ? db.GetByProc<SelectListItem>("GetActionReasons", parameters: new { ActionCode = pd.ActionCode })
                : new List<SelectListItem>(); // ActionReasons n/a for PositionActions
            ParSelectors.PrependBlankSelection(ActionReasons);
            ViewMode = "edit"; // default to edit mode
            StartingWorkflow = false;
            ApprovalWorkflowVm = new ApprovalWorkflowVm(pd.ApprovalWorkflowId);
            // pre-validation:
//            ValErr = new ModelStateDictionary();
            ValErr = new Dictionary<string, string>();
            var results = new List<ValidationResult>();
            ValidationContext vc = new ValidationContext(pd);
            foreach (PropertyInfo pi in pd.GetType().GetProperties())
            {
                var va = pi.GetCustomAttributes<ValidationAttribute>();
                if (va.Count<ValidationAttribute>() != 0)
                {
                    var result = Validator.TryValidateValue(pi.GetValue(pd), vc, results, va);
                    if (!result)
                    { // validation failed
//                        ValErr.AddModelError(pi.Name, results[results.Count - 1].ErrorMessage);
                        ValErr[pi.Name] = results[results.Count - 1].ErrorMessage;
                    }
                }
            }
        }

        public ApprovalWorkflowVm ApprovalWorkflowVm { get; set; }
        
        public ParDetail Par {
            get
            {
                return pd;
            }
        }

        public CurrentUserInfo CurrentUser { get; set; }

//        public ParDetail Update(bool validate)
        public bool Update(bool validate)
        {
            //if (Par.StatusId.HasValue && Par.StatusId == 4)
            if (Par.UpdateAction == "Process")
            {
                // status set to Processed...begin processing.
                bool success = false;
                try {
                    success = db.Exec("ProcessPar", new { ParId = Par.Id });
                    return success;
                    //if (success)
                    //{
                    //    return Par;
                    //}
                    throw new Exception("Must be approved and unprocessed before processing.");
                }
                catch (Exception xcp) {
                    throw new Exception("Process PAR failed: " + xcp.Message);
                }
            }

            // prevent changing if approval process has begun and non-HR/Admin...            

            List<string> udp = new List<string> {
                "ActionReasonId",
                "ApptTimeMgtStatus",
                "ApptPayScaleEffectiveDate",
                "ApptPayScaleReason",
                "ApptPayScaleTypeArea",
                "ApptPayScaleGroup",
                "ApptPayScaleLevel",
                "ApptPayAnnualSalary",
                "ApptPayHourlyRate",
                "ApptShiftDifferential",
                "ApptStandbyPay",
                "ApptPayNextIncrease",
                "ApptStepMEligibleDate",
                "ApptPayCapUtilityLevel",
                //"ApptShiftDesignation",
                "ApptWorkHoursWk1",
                "ApptWorkHoursWk2",
               // "ContractMonths", REMOVED
                "ContractTypeCode",
                "ContractEndDate",
                "DeactivateClassCode",
                "EmpAddrStreetLine1",
                "EmpAddrStreetLine2",
                "EmpAddrCity",
                "EmpAddrState",
                "EmpAddrZipCode",
                "EmpAddrCountyCode",
                "EmpAddrHomePhone",
                "EmpDOB",
                "EmpEmail",
                "EmpGender",
                "EmpFirstName",
                "EmpHrmsId",
                "EmpLastName",
                "EmpMiddleName",
                "EmpPreferredName",
                "EmpSuffix",
                "EmpSSN",
                "EmpDS01_Anniversary",
                "EmpDS02_Appointment",
                "EmpDS03_CtsHire",
                "EmpDS04_PriorPid",
                "EmpDS05_Seniority",
                "EmpDS07_UnbrokenService",
                "EmpDS09_VacLeaveFrozen",
                "EmpDS18_PersonalHolidayElg",
                "EmpDS26_PersonalLeaveElg",
                //"EmpDS27_TsrLeaveElg", // REMOVED property
                "EmpRaceCodes",
                "EmpHispanic",
                "EmpMilitaryStatus",
                "EmpMilitaryBranch",
                "EmpDisability",
                "EffectiveDate",
                "PercentFullTime",
                "FillLevel",
                "Notes",
                "WorkScheduleCode",
                "PosGeneralDescription",
                "PosWorkingTitle",
                "PosOrgUnitId",
                "PosCostCenter",
                "PosJobId",
                "PosSupervisorPosId",
                "PosIsInTraining",
                "PosPersonnelSubArea",
                "PosReqsBackgroundCheck",
                "PosWorkersCompCode",
                "PosIsSupervisor",
                "PosIsManager",
                "PosIsAppointingAuthority",
                "PosPayGradeTypeArea",
                "PosPayGrade",
                "PosEeGroup",
                "PosEeSubgroup",
                "PosIsSection4",
                "PosIsTandem",
                "PosLocationId",
                "PosWorkScheduleCode",
                //"PosShiftDesignation",
                "PosShiftCode",
                "PosShiftDiffEligible",
                "PosStandbyEligible",
                "PosJvac",
                "PosPointValue",
                "StatusId",
                "FromAgencyCode",
                "IsTransferIn",
                "TargetAgencyCode",
                "IsTransferOut",
                "IsPayChangeOnly",
                "ApptFillOtEligCode"
            };
            if (Par.ActionCode == "U3" && !Par.IsPayChangeOnly && !Par.IsTransferIn && !Par.IsTransferOut)
            {
                udp.Add("EmployeeId");
            }

            // TODO: convert non-nullable fields to defaults or make nullable
            if (Par.Notes == null) // Notes is defined as not null...
            {
                Par.Notes = String.Empty;
            }
            // EmployeeId of 0 is changed to null in sproc...
            db.Pars.Attach(Par);
            var entry = db.Entry(Par);
            foreach (var p in udp)
            {
                entry.Property(p).IsModified = true;
            }
            // without disabling ValidateOnSave,
            // SaveChanges re-invokes ParDetailValidator (also invoked implicitly during controller method arg coercion)
            // http://stackoverflow.com/questions/8099949/entity-framework-mvc3-temporarily-disable-validation
            //db.Configuration.ValidateOnSaveEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = validate;
            try
            {
                var saveCount = db.SaveChanges();
            }
            //catch (Exception xcp)
            catch (System.Data.Entity.Validation.DbEntityValidationException xcp)
            {
                // TODO: set, return ModelState here or just don't call this if ModelState is invalid... 
                throw new Exception("save failed: " + xcp.Message);
            }
            catch (Exception xcp)
            {
                throw new Exception("save failed: " + xcp.Message);
            }
//            return db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = Par.Id });
            return true;
        }

        // TODO: convert this to Par table/class, replacing ApptActionRequest + Employee...
        //public ParDetail UpdateOLD()
        //{
        //    String[] udp = new String[] {
        //        "ActionReasonId",
        //        "ContractMonths",
        //        "ContractTypeCode",
        //        "EffectiveDate",
        //        "EmploymentPercent",
        //        "FillLevel",
        //        "Notes",
        //        "WorkScheduleCode"
        //    };

        //    // TODO: change ApptActionRequest class to generic Par?
        //    var aar = new ApptActionRequest()
        //    {
        //        Id = Par.Id,
        //        ActionReasonId = Par.ActionReasonId,
        //        ContractMonths = Par.ContractMonths,
        //        ContractTypeCode = Par.ContractTypeCode,
        //        EffectiveDate = Par.EffectiveDate,
        //        EmploymentPercent = Par.PercentFullTime,
        //        FillLevel = Par.FillLevel,
        //        Notes = Par.Notes,
        //        WorkScheduleCode = Par.WorkScheduleCode
        //        //ActionReasonId = Par. (may need to add this property)
        //    };

        //    db.Requests.Attach(aar);
        //    var entry = db.Entry(aar);
        //    foreach (var p in udp)
        //    {
        //        // TODO: check whether properties have actually changed?
        //        // or just log all values to audit table in trigger.
        //        entry.Property(p).IsModified = true;
        //        //entry.Property(e => e.Email).IsModified = true;
        //    }

        //    // TODO: consider making Employee a property of ParDetail, then just grab Par.Emp.
        //    // TODO: stop pre-creating Employees before Par approval.  Just capture emp data in Par.
        //    var emp = new Employee()
        //    {
        //        Id = Par.EmployeeId,
        //        HrmsPersonId = Par.EmpHrmsId, // 0 for New Hires...
        //        FirstName = Par.EmpFirstName,
        //        LastName = Par.EmpLastName,
        //        MiddleName = Par.EmpMiddleName,
        //        PreferredName = Par.EmpPreferredName,
        //        Suffix = Par.EmpSuffix,
        //        Gender = Par.EmpGender,
        //        LastUpdated = DateTime.Now,
        //        DOB = Par.EmpDOB
        //    };
        //    var eud = new string[] {
        //        "FirstName",
        //        "LastName",
        //        "MiddleName",
        //        "PreferredName",
        //        "Suffix",
        //        "Gender",
        //        "LastUpdated",
        //        "DOB"
        //    };

        //    db.Employees.Attach(emp);
        //    var e2 = db.Entry(emp);
        //    foreach (var p in eud)
        //    {
        //        e2.Property(p).IsModified = true;
        //    }

        //    var saved = db.SaveChanges();
        //    return db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = Par.Id });
        //}

        // TODO: enum? read/create/update...
        public string ViewMode { get; set; }

        // set by AuthProvider:
        public bool CanDelete { get; set; }

        //used when first activating workflow
        public bool StartingWorkflow { get; set; }

//        public ModelStateDictionary ValErr { get; set; }
        public Dictionary<string,string> ValErr { get; set; }

        [ScriptIgnore]
        public ParSelectors Selectors { get; set; }
        // this is here instead of selectors since it varies by Action.
        [ScriptIgnore]
        public List<SelectListItem> ActionReasons { get; set; }
    }
}