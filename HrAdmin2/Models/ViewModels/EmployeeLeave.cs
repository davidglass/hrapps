﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class EmployeeLeave : ModelBase
    {

       public DateTime EffectiveDate {get; set;}
       public Decimal LeaveType { get; set; }
       public string LeaveTypeDesc { get; set; }
       public DateTime BeginDate  {get; set;}
       public DateTime   EndDate  {get; set;}
       public String BeginningBalance  {get; set;}
       public String LeaveEarned { get; set; }
       public String LeaveTaken { get; set; }
       public String LeavePaid { get; set; }
       public String LeaveAdj { get; set; }
       public String LeaveDonated { get; set; }
       public String LeaveReturned { get; set; }
       public String LeaveReceived { get; set; }
       public String EndBalance { get; set; }
    }
}