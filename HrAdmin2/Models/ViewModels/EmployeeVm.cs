﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class EmployeeVm : BaseVm
    {
        public EmployeeDetail Emp { get; set; }
        public List<Appointment> ApptHistory { get; set;}
    }
}