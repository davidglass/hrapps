﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalTaskVm
    {
        
        private HrAdminDbContext db;

        public ApprovalTask Task { get; set; }
        public int ApproverPositionEmployeeId { get; set; }
        public string ApproverPositionTitle {get; set;}
		public string ApproveGroupName {get; set;}	
		public string FinalApproverPersonLastName {get; set;}
		public string FinalApproverPersonPreferredName {get; set;}
		public string ApproverPositionPersonLastName {get; set;}
        public string ApproverPositionPersonPreferredName { get; set; }
        public string ApproverPositionPersonEmail { get; set; }
        public bool ApproverPositionIsAppointingAuth { get; set; }
        public bool Required { get; set; }
        public bool Authorized { get; set; }

        //TODO
        public List<Employee> ActiveDelegates { get; set; }

        public bool HasDelegates
        {
            get
            {
                return ActiveDelegates.Count > 0;
            }
        }

        public bool AssignedToGroup
        {
            get
            {
                return !String.IsNullOrEmpty(ApproveGroupName) && String.IsNullOrEmpty(ApproverPositionTitle);
            }
        }

        public String Status
        {
            get
            {
                if (Task.StatusCode == ApprovalWorkflow.statusCode.Canceled)
                    return "Skipped";

                return Task.StatusCode.ToString();
            }
        }

        public String FinalApproverPersonName
        {
            get
            {
                return FinalApproverPersonLastName + ", " + FinalApproverPersonPreferredName;
            }
        }

        public String CompletedDateString
        {
            get
            {
                //if (Task.CompletedDate == null) return "";
                //return Task.CompletedDate.ToString("MM/dd/yy");
                // changed to nullable - DG
                return Task.CompletedDate.HasValue ? Task.CompletedDate.Value.ToString("MM/dd/yy") : "";
//                return Task.CompletedDate.ToString("MM/dd/yy");
            }
        }

        public ApprovalTaskVm() { Task = new ApprovalTask(); Required = false; ActiveDelegates = new List<Employee>(); }

        public ApprovalTaskVm(ApprovalTask t, HrAdminDbContext db)
        {
            this.db = db;
            Task = t;
            Init(Task.Id);
            
        }

        private void Init(int id)
        {
            Required = false;
            Authorized = false;//default
            ApprovalTaskVm tempTask = db.GetSingleByProc<ApprovalTaskVm>("GetApprovalTaskVm",
                parameters: new { Id = id });

            ApproverPositionPersonLastName = tempTask.ApproverPositionPersonLastName;
            ApproverPositionPersonPreferredName = tempTask.ApproverPositionPersonPreferredName;
            ApproverPositionTitle = tempTask.ApproverPositionTitle;
            ApproveGroupName = tempTask.ApproveGroupName;
            FinalApproverPersonLastName = tempTask.FinalApproverPersonLastName;
            FinalApproverPersonPreferredName = tempTask.FinalApproverPersonPreferredName;
            ApproverPositionIsAppointingAuth = tempTask.ApproverPositionIsAppointingAuth;
            ApproverPositionPersonEmail = tempTask.ApproverPositionPersonEmail;
            ApproverPositionEmployeeId = tempTask.ApproverPositionEmployeeId;

            ActiveDelegates = db.GetByProc<Employee>("GetEmployeesDelegatedToTask",
                parameters: new { TaskId = id });
        }
    }
}