﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class AttachmentInfoVm
    {

        public int AttachmentId { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public long SizeInBytes { get; set; }
        public string Size
        {
            get
            {
                return ConvertBytes(SizeInBytes);
            }
        }
        public string MimeType { get; set; }
        public Attachment Content { get; set; }

        public AttachmentInfoVm() { }


        //http://www.java2s.com/Code/CSharp/File-Stream/Converttoeasytoreadbytevalue.htm
        private string ConvertBytes(long byteCount)
        {
            string size = "0 Bytes";
            if (byteCount >= 1073741824)
                size = String.Format("{0:##.##}", byteCount / 1073741824) + " GB";
            else if (byteCount >= 1048576)
                size = String.Format("{0:##.##}", byteCount / 1048576) + " MB";
            else if (byteCount >= 1024)
                size = String.Format("{0:##.##}", byteCount / 1024) + " KB";
            else if (byteCount > 0 && byteCount < 1024)
                size = byteCount.ToString() + " Bytes";

            return size;
        }

    }
}