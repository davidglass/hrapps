﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class OrgNodeJsTree
    {
        private OrgNode model;
        // list constructor, assumes item[0] is root.
        public OrgNodeJsTree(List<OrgNode> nodes)
        {
            // this could all be done in the constructor I suppose...
            Init(nodes[0]);
            BuildTree(nodes);
        }

        // private individual node constructor, used by BuildTree
        private OrgNodeJsTree(OrgNode n)
        {
            Init(n);
        }

        private void Init(OrgNode n)
        {
            model = n;
            attr["id"] = n.Id;  // this may need a uniquifier to distinguish PositionId, OrgUnitId...
            data["title"] = metadata["title"] = model.Name; // duplicating to metadata for easy access...
            var apppath = HttpContext.Current.Request.ApplicationPath;
            data["icon"] = (model.NodeType == "Position") ?
                // leading slash needed unless root:
//                HttpContext.Current.Request.ApplicationPath + "/Scripts/jstree/themes/default/system-users.png" : null;
(apppath.Equals("/") ? "" : apppath) + "/Scripts/jstree/themes/default/system-users.png" : null;
            metadata["classcode"] = n.ClassCode;
            metadata["HrmsJobId"] = n.HrmsJobId;
            metadata["Incumbent"] = n.Incumbent;
            metadata["IncumbentFirst"] = n.IncumbentFirst;
            metadata["IncumbentId"] = n.IncumbentId;
            metadata["lazy"] = (n.SubUnitCount + n.PositionCount) > 0 && !n.Expanded;
            metadata["pcount"] = n.PositionCount;
            metadata["nt"] = n.NodeType;
            metadata["sv"] = n.IsSupervisor;
            metadata["vac"] = n.IsVacant;
            // lazy property is removed after lazy load completes, preventing multiple lazy-loads on re-expand.
        }

        private void BuildTree(List<OrgNode> nodes)
        {
            var nt = new List<OrgNodeJsTree>(); // nesting tracker
            nt.Add(this);
            int rootlevel = model.Depth;
            int level = rootlevel;
            for (int i = 1; i < nodes.Count; i++)
            {
                var n = new OrgNodeJsTree(nodes[i]);
                var depth = n.model.Depth;
                if (depth > level)
                {
                    if (nt.Count >= depth - rootlevel + 1)
                    {
                        nt[depth - rootlevel] = n;
                    }
                    else
                    {
                        nt.Add(n);
                    }
                    nt[level - rootlevel].children.Add(nt[depth - rootlevel]);
                }
                else if (depth <= level && depth > rootlevel)
                {
                    nt[depth - rootlevel] = n;
                    nt[depth - rootlevel - 1].children.Add(n);
                }
                // this should only happen if multiple roots are allowed.
                else if (depth == rootlevel)
                {
                    //tl.Add(n);
                    nt[0] = n;
                }
                level = depth;
            }
        }

        // public properties, must match JSON_DATA plugin requirements:
        // http://www.jstree.com/documentation/json_data
        //public string data
        //{
        //    get
        //    {
        //        return model.Name;
        //    }
        //}

        public string state {
            get {
                return (model.PositionCount + model.SubUnitCount) == 0 ? "leaf" : model.Expanded ? "open" : "closed";
            }
        }

        public Dictionary<string, object> data = new Dictionary<string, object>();
        public Dictionary<string, object> attr = new Dictionary<string, object>();
        public List<OrgNodeJsTree> children = new List<OrgNodeJsTree>();
        public Dictionary<string, object> metadata = new Dictionary<string, object>();
    }
}