﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalAssignedDelegateVm
    {

        private HrAdminDbContext db;

        public List<ApprovalParVm> DelegatedParApprovals { get; set; }

        public ApprovalDelegateVm DelegateVm { get; set; }
        
        public string DelegatedApprovalsCount
        {
            get
            {
                return DelegatedParApprovals.Count.ToString();
            }
        }

        public ApprovalAssignedDelegateVm()
        {
            DelegatedParApprovals = new List<ApprovalParVm>();
            DelegateVm = new ApprovalDelegateVm();
        }

        public ApprovalAssignedDelegateVm(ApprovalDelegateVm delegateVm, HrAdminDbContext dbctx)
        {
            DelegateVm = delegateVm;
            db = dbctx;
            Init();
        }

        private void Init()
        {
            DelegatedParApprovals = new List<ApprovalParVm>();

            //get PARs if delegate is for PAR
            if ((DelegateVm.Delegate.Apps.Where(d => d.Id == (int)ApprovalWorkflow.approvalApp.PAR)).Count() > 0)
            {
                //gets active PAR approvals
                DelegatedParApprovals = db.GetByProc<ApprovalParVm>("GetApprovalParVms",
                    parameters: new { ApproverPositionId = DelegateVm.Delegate.DelegatedPositionId });

                //load ApprovalTaskVms based off Ids.
                foreach (var approval in DelegatedParApprovals)
                {
                    approval.LoadTaskData();
                }
            }
        }
    }
}