﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrAdmin2.Models.ViewModels
{
    public class DelegationListVm : BaseVm
    {
        private HrAdminDbContext db;

        public DelegationVm DelegationVm { get; set; }

        //used for drop down list when creating new delegate
        public List<SelectListItem> CurrentEmp { get; set; }

        //used for drop down list when creating new delegate
        public List<SelectListItem> CurrentPos { get; set; }

        //used for drop down list when creating new delegate
        public List<SelectListItem> CurrentPosOfEmp { get; set; }

        public DelegationListVm(HrAdminDbContext dbctx)
        {
            db = dbctx;
            //model = p;
            try
            {
                Init();
            }
            catch
            {

            }
        }

        public DelegationListVm()
        {
            db = new HrAdminDbContext();
            //model = p;
            try
            {
                Init();
            }
            catch
            {

            }
        }


        private void Init()
        {
            DelegationVm = new DelegationVm(db);

            //load employee select list for delegate creation
            CurrentEmp = new List<SelectListItem>();
            CurrentEmp = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", parameters: new { });

            CurrentPos = new List<SelectListItem>();
            CurrentPos = db.GetByProc<SelectListItem>("GetCurrentPositionSelection", parameters: new { });

            CurrentPosOfEmp = new List<SelectListItem>();
            CurrentPosOfEmp = db.GetByProc<SelectListItem>("GetPositionOfCurrentEmpSelection", parameters: new { });
            
        }
    }
}