﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrAdmin2.Models.ViewModels
{
    public class DelegationVm
    {
        private HrAdminDbContext db;

        public List<ApprovalDelegateVm> Results { get; set; }

        //public DelegationQuery Query { get; set; }

        public List<ApprovalDelegateVm> NewDelegates { get; set; }        

        public Position ApproverPosition { get; set; }

        //first employee match to position in appointment table
        public Employee ApproverEmployee { get; set; }

        public string NewDelegateCount
        {
            get
            {
                return NewDelegates.Count.ToString();
            }
        }

        public string DelegateCount
        {
            get
            {
                return Results.Count.ToString();
            }
        }

   
        public DelegationVm(HrAdminDbContext dbctx)
        {
            db = dbctx;
            //model = p;
            try
            {
                Init();
            }
            catch
            {

            }
        }

        public DelegationVm()
        {
            db = new HrAdminDbContext();
            //model = p;
            try
            {
                Init();
            }
            catch
            {

            }
        }

        private void Init()
        {
            NewDelegates = new List<ApprovalDelegateVm>();
            
            Results = new List<ApprovalDelegateVm>();


            List<ApprovalDelegate> delegates = db.GetByProc<ApprovalDelegate>("GetApprovalDelegates", parameters: new { });

            foreach (var del in delegates)
            {
                //get app list
                del.Apps = db.GetByProc<ApprovalApp>("GetApprovalDelegateApps",
                    parameters: new { DelegateId = del.Id });

                Results.Add(new ApprovalDelegateVm(del, db));

                //if (del.StartDate > DateTime.Now)
                //{
                //    FutureDelegates.Add(new ApprovalDelegateVm(del, db));
                //}
                //else
                //{
                //    ActiveDelegates.Add(new ApprovalDelegateVm(del, db));
                //}
            }
            
        }
    }

    //public class DelegationQuery
    //{
    //    public string ActionCode { get; set; }
    //    public int OrgUnitId { get; set; }
    //    public int ParStatusId { get; set; }
    //    public int WorkflowStatusId { get; set; }
    //    public DateTime? FromDate { get; set; }
    //    public DateTime? ToDate { get; set; }
    //    public string Query { get; set; }
    //}
}