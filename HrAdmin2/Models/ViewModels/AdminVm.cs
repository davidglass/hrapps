﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class AdminVm : BaseVm
    {
        private HrAdminDbContext dbc;
        // moved ExpiringContracts to Api/ExpirationController...
        //public List<ExpiringContract> ExpiringContracts { get; set; }
        public List<ExpiringDelegate> ExpiringDelegates { get; set; }
//        public AdminVm(HrAdminDbContext db)
        public AdminVm()
        {
            //dbc = db;
            //Init();  
        }

        //private void Init()
        //{
        //    // these were moved to ApiControllers
        //    //ExpiringContracts = dbc.GetByProc<ExpiringContract>("GetExpiringContracts", new { });
        //    //ExpiringDelegates = dbc.GetByProc<ExpiringDelegate>("GetExpiringDelegates", new { });
        //}
    }

    public class ExpiringDelegate
    {
        public int DelegatedPositionId { get; set; }
        public string DelegatedName { get; set; }
        public int DelegatedToPersonId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DaysLeft { get; set; }
        public string DelegatedToName { get; set; }
    }
}