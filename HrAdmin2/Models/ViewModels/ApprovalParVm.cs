﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalParVm
    {
        private HrAdminDbContext db;
        
        public int ParId {get; set;}
	    public int ApprovalWorkflowId {get; set;}
	    public int ApprovalTaskId {get; set;}
	    public int ParEmployeeId {get; set;}
        public int ParPositionId {get; set;}
        public DateTime EffectiveDate { get; set; }
        public string SupervisorName { get; set; }
	    public string ParActionCode {get; set;}
	    public string ParActionReason {get; set;}
        public string ParEmployeePreferredName {get; set;}
	    public string ParEmployeeLastName {get; set;}
        public string ParPosWorkingTitle { get; set; }

        public ApprovalTaskVm ApprovalTaskVm { get; set; }


        public String EffectiveDateString
        {
            get
            {
                return EffectiveDate.ToString("MM/dd/yy");
            }
        }

        public ApprovalParVm() { EffectiveDate = new DateTime(); }

        public void LoadTaskData()
        {
            try
            {
                db = new HrAdminDbContext();
                ApprovalTask task = db.GetSingleByProc<ApprovalTask>("GetApprovalTask",
                    parameters: new { Id = ApprovalTaskId });
                ApprovalTaskVm = new ApprovalTaskVm(task, db);

                //also get employee info of position
                var EmployeeList = db.GetByProc<Employee>("GetEmployeeIdOfPosition",
                    parameters: new { PositionId = ParPositionId });
                if (EmployeeList.Count > 0)
                {
                    ParEmployeeId = EmployeeList[0].Id;
                    ParEmployeeLastName = EmployeeList[0].LastName;
                    ParEmployeePreferredName = EmployeeList[0].PreferredName;
                }

                SetSupervisor(ParPositionId);
            }
            catch { }
        }

        protected void SetSupervisor(int positionId)
        {
            //find supervisor of position
            //List starts with position number at top of org chart and works down to starting position
            List<ChainLink> positionIds = db.GetByProc<ChainLink>("dbo.GetChainOfCommand", new { Id = positionId });
            //chain is in reverse order
            positionIds.Reverse();
            if (positionIds.Count != 1)
            {
                //remove first member of chain which is requestor. 
                positionIds.RemoveAt(0);
            }
            else
            {
                //special case for director
                SupervisorName = "Director";
                return;
            }

            //create each task, and write to database
            foreach (ChainLink approvalPostion in positionIds)
            {
                var EmployeeList = db.GetByProc<Employee>("GetEmployeeIdOfPosition",
                    parameters: new { PositionId = approvalPostion.Id });
                if (EmployeeList.Count > 0)
                {
                    SupervisorName = EmployeeList[0].PreferredName + " " + EmployeeList[0].LastName;
                    break;
                }
            }
        }
    }
}