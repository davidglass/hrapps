﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalEmailVm : ApprovalParVm
    {
        private HrAdminDbContext db;

        public string UrlPath { get; set; }
        //public string UrlPath = WebConfigurationManager.AppSettings["UrlPrefix"];
        //public string UrlPath = "http://localhost/HrAdmin2/";
        //public string UrlPath = HttpContext.Current.Server.MapPath("~");

        public int EmployeePositionIdSentTo { get; set; }
        public string EmailToAddress { get; set; }
        public string EmailToName { get; set; }
        public string EmailToPreferedName { get; set; }


        public ApprovalEmailVm() { EffectiveDate = new DateTime();}

        public void LoadDefaultEmailData()
        {
            try
            {
                //Assign default position id should never be null, (unless assigned to group?)
                EmployeePositionIdSentTo = ApprovalTaskVm.Task.ApproverPositionId.GetValueOrDefault(-1);
                EmailToName = ApprovalTaskVm.ApproverPositionPersonLastName + ", " + ApprovalTaskVm.ApproverPositionPersonPreferredName;
                EmailToPreferedName = ApprovalTaskVm.ApproverPositionPersonPreferredName;
                //TODO: not working?                
                EmailToAddress = ApprovalTaskVm.ApproverPositionPersonEmail;
            }
            catch { }
        }



    }
}