﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ActionRequestVm
    {
        private ApptActionRequest model;
        public ActionRequestVm(ApptActionRequest ar)
        {
            model = ar;
        }

        public int Id
        {
            get
            {
                return model.Id;
            }
        }

        public string ActionCode {
            get
            {
                return model.ActionCode;
            }
        }

        public int ActionReasonId
        {
            get
            {
                return model.ActionReasonId;
            }
        }

        public string EffectiveDate {
            get
            {
                return model.EffectiveDate.ToString("MM/dd/yy");
            }
        }

        public string Notes {
            get
            {
                return model.Notes;
            }
        }


        public int EmpId
        {
            get
            {
                return model.EmployeeId.HasValue ? model.EmployeeId.Value : 0;
            }
        }

        public int? FillLevel
        {
            get
            {
                return model.FillLevel;
//                return model.Appt != null ? model.Appt.FilledAsJobId : 0;
            }
        }

        public string ContractTypeCode
        {
            get
            {
                return model.ContractTypeCode;
            }
        }

        public int ContractMonths
        {
            get
            {
                return model.ContractMonths;
            }
        }

        public decimal EmploymentPercent
        {
            get
            {
                return model.EmploymentPercent;
            }
        }

        public List<AttachmentInfoVm> Attachments { get; set; }

        public bool Update(HrAdminDbContext ctx)
        {
            // just using built-in EF update on AAR for now:
            // see this discussion for update without fetch:
            // http://stackoverflow.com/questions/15336248/entity-framework-5-updating-a-record
            // TODO: handle attempts to update non-existent requests (Id==0) more gracefully. (should create first)
            // var aar = ctx.Requests.Find(Id);
            // preserve original create date:
            //model.CreateDate = aar.CreateDate;
            //ctx.Entry(aar).CurrentValues.SetValues(model);
            // update only updateable properties, ignoring others:
            String[] udp = new String[] {
                "ActionReasonId",
                "EffectiveDate",
                "FillLevel",
                "ContractTypeCode",
                "ContractMonths",
                "WorkScheduleCode",
                "EmploymentPercent",
                "Notes"
            };
            ctx.Requests.Attach(model);
            var entry = ctx.Entry(model);
            foreach (var p in udp)
            {
                // TODO: check whether properties have actually changed?
                // or just log all values to audit table in trigger.
                entry.Property(p).IsModified = true;
                //entry.Property(e => e.Email).IsModified = true;
            }
            var saved = ctx.SaveChanges();
            return saved == 1;
            // need to update both ActionRequest *and* associated elements...
            //return ctx.Exec("UpdateActionRequest", parameters: new {
            //    Id = Id,
            //    FillLevel = FillLevelJobId,
            //    ActionReasonId = model.ActionReasonId
            //});
        }
    }
}