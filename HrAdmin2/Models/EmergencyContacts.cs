﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class EmergencyContacts : ModelBase
    {
       
        public string Member { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string TelephoneNumber { get; set; }
       
    }
}