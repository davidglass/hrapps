﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    // TODO: convert this to BaseActiveRecord, with built-in CRUD capabilities as static methods
    public class ModelBase
    {
        public int Id { get; set; }
        public bool Validate { get; set; }
        public int? LastUpdatedByUser { get; set; }
    }
}