﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; // KeyAttribute

namespace HrAdmin2.Models
{
    public class PositionAppointment
    {

        //added id for row in search grid. Needs unique Id for object, with property name of "id"
        public string id
        {
            get
            {
                return FullName + StartDate.ToString() + PosId + EmpId;
            }
        }
        // "if your ID field is not named "Id" you have to add the [Key] attribute:
        [Key]
        public int PosId { get; set; }
        public int OrgUnitId { get; set; }
        public int EmpId { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name="Job Class")]
        public string JobTitle { get; set; }
        public bool IsVacant { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OrgUnitDesc { get; set; }

        public string StartDateJson
        {
            get
            {
                //return miliseconds so that date can be easily sorted and recreated in javascript for formating.
                //http://stackoverflow.com/questions/5955883/datetimes-representation-in-milliseconds
                return StartDate.ToUniversalTime().Subtract(
                    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString();
            }
        }

        public string EndDateJson
        {
            get
            {
                return EndDate.ToUniversalTime().Subtract(
                    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString();
            }
        }

        public string FullName
        {
            get
            {
                return LastName + ", " + FirstName;
            }
        }
    }
}