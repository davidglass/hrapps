﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class EmpContractEmailInfo
    {
        public int EmpId { get; set; } // explicit since not subclassing ModelBase...
        public string EmpName { get; set; }
        public string SupervisorName { get; set; }
        public int PositionId { get; set; }
        public string WorkingTitle { get; set; }
        public string WorkContractTypeDesc { get; set; }
        public DateTime ApptBegan { get; set; }
        public DateTime ContractEndDate { get; set; }
    }
}