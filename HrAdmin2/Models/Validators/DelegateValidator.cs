﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HrAdmin2.Models.Validators
{



    public class DelegateIdValidator
    {
        public static ValidationResult Validate(int id, ValidationContext vc)
        {            
            if (id < 1)
            {
                return new ValidationResult("HRMS ID is required for Rehire actions");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    public class DelegateRequiredDate
    {
        public static ValidationResult Validate(DateTime? val, ValidationContext vc)
        {
            if (!val.HasValue)
            {
                return new ValidationResult("A value is required.");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}