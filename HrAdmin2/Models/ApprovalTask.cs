﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ApprovalTask
    {

        public enum taskCode
        {
            MoveFwdRegardless = 1,
            Standard = 2,
            SignOff = 3
        }

        public ApprovalTask()
        {
            Comment = "";
        }


        public int Id { get; set; }
        public int? ApproverPositionId { get; set; }
        public ApprovalWorkflow.statusCode StatusCode { get; set; }
        public taskCode TaskCode { get; set; }
        public int? FinalApproverPersonId { get; set; }
        public int? ApproveGroupId { get; set; }
        public int? WorkflowOrder { get; set; }
        // changed DateTime to nullable -DG
        public DateTime? CompletedDate { get; set; }
        public string Comment { get; set; }
        public string FinalApproverPersonLanId { get; set; }

        //public ApproveGroup ApproveGroup { get; set; }

    }
}