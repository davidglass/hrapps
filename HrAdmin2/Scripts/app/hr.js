﻿angular.module('hr', ['kendo.directives', 'ngRoute'])
//.controller('PdfController', function ($scope, $filter, $location, $routeParams, id) {
.controller('PdfController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    $scope.vm = fetch.data;
    document.title = "PDF #" + $scope.vm.Description.Id + " Detail";
    //alert('vm: ' + JSON.stringify($scope.vm.Duties));
    //    $scope.$routeParams = $routeParams; // pdfId is passed in $routeParams...

    // TODO: look up Pdf Detail from $routeParams.pdfId... for now just copying from parent scope.
    //alert('root scope working title: ' + $scope.$parent.vm.Pos.WorkingTitle);

    // TODO: this is stupid little demo...replace with actual Pdf detail lookup in RESOLVE of route.
    //var pdfs = $scope.$parent.vm.Pdfs;
    //for (var i = 0; i < pdfs.length; i++) {
    //    //if (pdfs[i].Id == $routeParams.pdfId) {
    //    if (pdfs[i].Id == id) {
    //        $scope.vm = pdfs[i]; // set vm to matching pdf record.
    //    }
    //}

    //alert('dvm:' + JSON.stringify(pdfDetailVm));
    $scope.options = {
        PositionType: {
            dataSource: [
                { Id: "1", Name: "Classified" },
                { Id: "2", Name: "Exempt" },
                { Id: "3", Name: "WMS" }
            ],
            dataValueField: "Id",
            dataTextField: "Name",
            optionLabel: " - select - "
        },
        JobClass: {
            dataSource: pdfDetailVm.JobClasses,
            //[
            //    { Id: "479J", Name: "479J - IT SPECIALIST 2" },
            //    { Id: "479K", Name: "479K - IT SPECIALIST 3" },
            //    { Id: "479L", Name: "479L - IT SPECIALIST 4" },
            //    { Id: "479M", Name: "479M - IT SPECIALIST 5" },
            //    { Id: "479N", Name: "479N - IT SYS/APP SPEC 6" }
            //],
            //dataValueField: "Id",
            //dataTextField: "Name",
            dataValueField: "Value",
            dataTextField: "Text",
            optionLabel: " - select - ",
            template: '<span class="options">#: Text#</span>'
        },
        // *note*, these are options for Kendo-UI widgets, not <option>s:
        // also note, DutyEditor should be data-bound to CurrentlyEditingDuty or similar in VM.
        DutyEditor: {
            encoded: false, // else Angular double-encodes
            tools: [
                "bold", "italic", "underline",
                "insertUnorderedList",
                "createLink", "unlink",
                // see SiteBasic.css, .k-save for example of setting icon from sprite.
                {
                    name: "save",
                    tooltip: "save changes",
                    exec: function (e) { // e is button click event...not sure whether we need it...
                        $scope.saveDuty();
                        //alert('saving selected duty info [TODO]...' + $scope.selectedDutyId);
                        //var ed = $(this).data("kendoEditor");
                    },
                    // templates can also be specified with variable substitution as text/x-kendo-template...
                    //template: "<button class='k-button'><span class='k-icon k-update'>&nbsp;</span>Save</button>"
                }
            ],
            // trying to fix bug where initially editing newitem, then selecting existing item disables editor:
            // seems to work, even though it is theoretically re-focusing an item that just lost focus.
            change: function () {
                this.focus();
            }
        },
        DutyPanelBar: {
            animation: {
                collapse: {
                    duration: 300
                    //effects: 'fadeOut'
                },
                expand: {
                    duration: 300,
                    //                effects: 'expandVertical fadeIn'
                    effects: 'expandVertical'
                }
            },
            collapse: function (pb) {
                // exit edit mode on collapse...
                if ($scope.editingDutyId == $(pb.item).attr('id')) {
                    $scope.$apply(function () {
                        $scope.editingDutyId = 0;
                    });
                }
            },
            //expand: function (pb) {
            //    $scope.$apply(function () {
            //        $scope.editingDutyId = $(pb.item).attr('id');
            //    });
            //},
            expandMode: "multiple",
            //activate: function () {
            //    $scope.hideTasks(); // hide any floating duty task "tooltips"
            //},
            select: function (pb) {
                $scope.$apply(function () {
                    $scope.editingDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyIndex = $(pb.item).index();
                });
            }
        },
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expandMode: "single"
        }
    };

    $scope.collapseAll = function () {
        $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };

    $scope.collapseDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "single";
        $scope.dutiesPanelBar.collapse('li');
    }

    $scope.createDuty = function () {
        // TODO: get Id or complete Duty object from server insert or use client-side GUID generator...
        // faking with random() for now.
        var newDuty = {
//            Id: Math.floor(Math.random() * (2000000000)),
            PositionDescriptionId: $scope.vm.Description.Id,
            Duty: 'New Duty',
            TasksHtml: '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>',
            TimePercent: 0
        };

        var posturl = appRoot + 'api/PositionDuty/';
        try {
            $scope.asyncOp = true;
            $scope.asyncMessage = "creating duty";
            $http({ method: 'POST', url: posturl, data: newDuty })
            .success(function (data) {
                // $timeout should automatically call $apply:
                $timeout(function () {
                    //$scope.$apply(function () {
                    $scope.vm.Duties.push(data);
                    //});
                    $scope.asyncMessage = "";
                    $scope.asyncOp = false;
                }, 500);
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                $scope.asyncOp = false;
            });
        }
        catch (xcp) {
            var err = xcp;
        }
        //$scope.vm.Duties.push(newDuty);
        //$scope.dutiesPanelBar.destroy();
        //$scope.dutiesPanelBar.insertAfter(
        //    [{
        //        text: "New Item",
        //        content: "TEXT CONTENT (COPY FROM RENDERED RTE)"
        //    }],
        //    "li:last-child"
        //    );
        // var dpb = $scope.dutiesPanelBar;
        // (resolved this via hard-coded attributes in LI template)
        //dpb.destroy(); // leaves styles intact, just removed click listeners...
        // re-create panel bar:
        // *NOTE*, this is a hack since kpb does not respond to Angular collection change event:
        //$scope.dutiesPanelBar = $('#DutiesPanelBar').kendoPanelBar($scope.options.DutyPanelBar);
        // need to recompile panelbar DOM:
        //var tpl = $('#DutiesPanelBar')[0].outerHTML;
        //var ct = $compile(tpl)($scope);
    }

    // this passes the index of the clicked item, more reliable than selectedDutyIndex...
    $scope.deleteDuty = function (index, evt) {
        var d = $scope.vm.Duties[index];
        evt.stopPropagation(); // prevent toggling expand/collapse...
        if (confirm('this will delete duty #' + d.Id)) {
            var posturl = appRoot + 'api/PositionDuty/' + d.Id;
            try {
                $http({ method: 'DELETE', url: posturl })
                    .success(function (deleted) {
                        $timeout(function () {
                            $scope.vm.Duties.splice(index, 1);
                        }, 300); // 300ms timeout to ensure deletion is visible...
                    })
                    .error(function (data, status, errors, config) {
                        if (data.error) { alert('ERROR: ' + data.error); };
                    });
            }
            catch (xcp) {
                alert('$http delete failed.');
            }
        }
    }

    $scope.dutyCheckKey = function ($event) {
        if ($event.which == 13) { // pressed return
            $scope.editingDutyId = 0;
        }
    }

    $scope.dutyPctSum = function () {
        duties = $scope.vm.Duties;
        var sum = 0;
        for (var i = 0; i < duties.length; i++) {
            if (duties[i].TimePercent > 0) {
                sum += new Number(duties[i].TimePercent);
            }
        }
        return sum;
    }

    $scope.editDuty = function (index, evt) {
        $scope.editingDuty = $scope.vm.Duties[index];
        if (evt.stopPropagation) {
            evt.stopPropagation();
        }
        else {
            evt.stopImmediatePropagation();
        }
    }

    $scope.editingDutyId = 0;
    //= null;

    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };

    $scope.expandDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "multiple";
        $scope.dutiesPanelBar.expand('li');
    }

    $scope.hideTasks = function () {
        $('#DutyTasksPreview').hide();
    };

    $scope.isDutySelected = function (item) {
        return ($scope.selectedDutyId && $scope.selectedDutyId == item.Id);
    }

    $scope.pctkeydown = function ($event) {
        // allow only digits (48-57, 96-105 keypad), backspace (8), delete(46), cursor left (37), right (39), tab (9), enter(13)
        if (!(
            ($event.which > 47 && $event.which < 58)
            || ($event.which > 95 && $event.which < 106)
            || $event.which == 8
            || $event.which == 9
            || $event.which == 13
            || $event.which == 37
            || $event.which == 39
            || $event.which == 46)
            || ($event.shiftKey && $event.which != 9) // allow shift+tab nav
            || $event.ctrlKey
            || $event.metaKey
            ) {
            $event.preventDefault();
        }
    }

    $scope.saveDuty = function () {
        // *NOTE*, with multi-expand accordion, save button may be clicked in non-selected panel...
        // TODO: handle that better, perhaps cause tool buttons to auto-select parent panel?
        // alternatively, could save changes to all Duties at once?...
        var d = $scope.vm.Duties[$scope.selectedDutyIndex];
        var posturl = appRoot + 'api/PositionDuty/' + d.Id;
        try {
            $scope.asyncOp = true;
            $scope.asyncMessage = "updating duty"
            $http({ method: 'PUT', url: posturl, data: d })
            .success(function (data) {
                $timeout(function () {
                    $scope.asyncMessage = "";
                    $scope.asyncOp = false;
                }, 500);
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                $scope.asyncOp = false;
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    }

    //$scope.selectDuty = function (item, $event) {
    //    $scope.hideTasks();
    //    $scope.selectedDuty = item;
    //    // this seems necessary in IE8 for some reason, else new editor is opened disabled.
    //    $("#dutyEdit").data("kendoEditor").focus();
    //};

    //$scope.selectedDutyId = 1234;
    $scope.showTasks = function (item, $event) {
        if (item.TasksHtml) { // skip if blank
            // TODO: calculate correct display position, accounting for scroll positions, viewport space to left or right, etc.
            $('#DutyTasksPreview').html(item.TasksHtml);
            $('#DutyTasksPreview').css('left', $event.pageX + 16); // 16px right of cursor
            $('#DutyTasksPreview').css('top', $event.pageY + 16); // show 16px below cursor
            $('#DutyTasksPreview').fadeIn(200);
        }
    }
    // this is too early; template is not recompiled.  Just hardcoded Kendo widget classes into template, works.
    //$scope.$watchCollection('vm.Duties', function (newval, oldval) {
    //    // if new count > old count, rebuild panelbar...
    //    // *NOTE*, this is a hack since Kendo PanelBar ignores collection changes...
    //    if (newval.length > oldval.length) {
    //        //alert('item added...');
    //        //$scope.dutiesPanelBar = $('#DutiesPanelBar').kendoPanelBar($scope.options.DutyPanelBar);
    //    }
    //});
})
.controller('PositionController', function ($scope, $filter, $http, $location, $rootScope, $route, $routeParams, $timeout) {
    //    $scope.cpath = $scope.path = ''; // initial path (appRoot/Position/{id})
    //$rootScope.$on('$locationChangeSuccess', function (event) {
    //    // TODO: single (flat) hash of all nodes by id
    //    $scope.path = $location.url(); // e.g. "/Contracts, /Delegations"
    //    $scope.cpath = $scope.path.substr(0, $scope.path.lastIndexOf('/'));
    //});
    // bypass init if vm already set:
    //alert('VM:' + $scope.vm); // this gets triggered TWICE on first load, is undefined initially.
    if (!$scope.vm) {
        $scope.vm = vm; // vm global is populated in .cshtml template
        //$scope.vm.PositionDescriptions = [
        //    {
        //        Id: 1038,
        //        WorkingTitle: 'Infrastructure Architect',
        //        JobId: 50000188,
        //        ProposedClassCode: '479N',
        //        ProposedClassTitle: 'IT SYSTEMS/APP SPEC 6',
        //        //            ApprovalDate: new Date(Date.parse('9/17/2013')),
        //        ApprovalDate: Date.parse('9/17/2013'),
        //        LegacyStatus: 'Approved'
        //    },
        //    {
        //        Id: 858,
        //        WorkingTitle: 'Infrastructure Architect',
        //        JobId: 50000188,
        //        ProposedClassCode: '479N',
        //        ProposedClassTitle: 'IT SYSTEMS/APP SPEC 6',
        //        ApprovalDate: Date.parse('4/11/2012'),
        //        LegacyStatus: 'Approved'
        //    }
        //];
        $scope.appRoot = appRoot; // set in _Layout_ng.cshtml

        // for remembering expansion state during ngRoute navigation...
        // TODO: store this in a cookie.
        $scope.expandedItems = {'PosInfo': true}; // store LI ids...default first panel to open.
        $scope.pbopts = { 'selectedItemId': null };
        $scope.selectedPdfId = null; // for holding PDF selection for actions like cloning, etc...
        $scope.selectedPdfAction = "none";
        $scope.asyncMessage = ""; // e.g. CLONING, DELETING etc.

//        $scope.selectedItemId = []; // this is a hack since changed scalar props are reverted during ngRoute follow...
        //TODO: convert CRUD ops to use ngResource and/or custom resource factory
        $scope.cloneLastPdf = function () {
            var clone = angular.copy($scope.vm.PositionDescriptions[0]);
            clone.ApprovalDate = undefined;
            clone.Status = 'Cloning...';
            $scope.vm.PositionDescriptions.unshift(clone);
            clone.isAdding = true;

            // TODO: connect this to back end Web API...this is just UI demo
            // (should be deep clone server-side...)
            // return new Id from back end asynchronously...
            $timeout(function () {
                $scope.asyncOp = false;
                clone.Status = 'New';
                clone.Id = Math.floor(Math.random() * 2000000000);
                clone.isAdding = false;
            }, 500);
        };

        $scope.clonePdf = function (i) {
            /* this clone is just UI mock:
            var clone = angular.copy($scope.vm.PositionDescriptions[i]);
            clone.ApprovalDate = undefined;
            clone.Id = clone.LegacyStatus = "Cloning...";
            $scope.vm.PositionDescriptions.unshift(clone);
            clone.isAdding = true;
            */
            var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id + '/clone'
            try {
                $scope.asyncOp = true;
                $scope.asyncMessage = "CLONING"
                $http({ method: 'POST', url: posturl })
                .success(function (clone) {
                    $timeout(function () {
                        $scope.vm.PositionDescriptions.unshift(clone);
                        $scope.selectedPdfId = clone.Id;
                        $scope.asyncOp = false;
                    }, 500);
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); };
                    $scope.asyncOp = false;
                });
            }
            catch (xcp) {
                var err = xcp;
            }

            // TODO: connect this to back end Web API...this is just UI demo
            // return Id from back end asynchronously...
            //$timeout(function () {
            //    $scope.asyncOp = false;
            //    clone.Status = 'New';
            //    clone.Id = Math.floor(Math.random() * 2000000000);
            //    clone.isAdding = false;
            //    clone.LegacyStatus = "Cloned";
            //}, 500);
        };

        $scope.deletePdf = function (i) {
            var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id
            // TODO: allow deleting only Pdfs in "created" approval state...
            if (!confirm('This will permanently delete PDF #' + $scope.vm.PositionDescriptions[i].Id
                + ', along with any associated Duty allocations.')) { return false; };
            // TODO: put this into AsyncOpIndicator...
            //$scope.vm.PositionDescriptions[i].Status = "Deleting...";
            $scope.vm.PositionDescriptions[i].isDeleting = true;
            $scope.asyncOp = true;
            $scope.asyncMessage = "DELETING"

            $http({ method: 'DELETE', url: posturl })
            .success(function (deleted) {
                // this seems to succeed silently for IE...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.vm.PositionDescriptions.splice(i, 1);
                }, 500); // 300ms timeout to ensure deletion is visible...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });

            //$timeout(function () {
            //    $scope.asyncOp = false;
            //    $scope.vm.PositionDescriptions.splice(i, 1);
            //}, 500);
        };

        $scope.doPdfAction = function (newscope, idx) {
            switch (newscope.selectedPdfAction) {
                case 'clone': {
                    $scope.clonePdf(idx);
                    return;
                }
                case 'delete': {
                    $scope.deletePdf(idx);
                    return;
                }
                default: return;
            }
//            alert('doing PDF action: ' + newscope.selectedPdfAction + ' pdf index # ' + idx);
        }

        // config options for KendoUI widgets:
        // TODO: move this out of if block, dynamically set expandMode to multiple only if > 1 currently expanded?
        //$scope.options = {
        //    PanelBar: {
        //        animation: {
        //            collapse: {
        //                duration: 300
        //            },
        //            expand: {
        //                duration: 300,
        //                effects: 'expandVertical'
        //            }
        //        },
        //        expandMode: ($scope.expandedItems.length) ? "multiple" : "single",
        //        //            expandMode: (kc == 2) ? "multiple" : "single",
        //        collapse: function (pb) {
        //            delete $scope.expandedItems[$(pb.item).attr('id')];
        //        },
        //        expand: function (pb) {
        //            $scope.expandedItems[$(pb.item).attr('id')] = true;
        //        },
        //        select: function (pb) {
        //            $scope.selectedItemId = $(pb.item).attr('id');
        //        }
        //    }
        //};

        $scope.performAction = function (selectorId) {
            var action = $('#' + selectorId).val();
            if (action != '') { // skip if no selection
                doAction(action, 'Pos'); // always Pos in this context, can vary in treeview context
            }
        }
        $scope.formatJsonDate = function (d) { return formatJsonDate(d) };

        $scope.rowClicked = function (pdr) {
            $scope.selectedPdfId = pdr.Id;
        }
    }

    document.title = 'Position #' + $scope.vm.Pos.Id + ' Detail';

    $scope.options = {
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expandMode: $scope.pbopts.expandMode || 'single',
            collapse: function (pb) {
                delete $scope.expandedItems[$(pb.item).attr('id')];
            },
            expand: function (pb) {
                $scope.expandedItems[$(pb.item).attr('id')] = true;
            },
            select: function (pb) {
                $scope.pbopts.selectedItemId = $(pb.item).attr('id');
            }
        }
    };

    // these always needs initialization due to panelbar directive:
    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };

    $scope.collapseAll = function () {
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };
})
.controller('SearchController', function ($http, $scope, $timeout) {
    // vm is server-generated vm, may include ValErr, Par, etc.
    $scope.vm = vm; // vm global is populated in .cshtml template
    $scope.sortField = 'LastName'; // default sort field
    $scope.desc = false;
    $scope.sortOn = function (f) {
        //*note* Angular can take array of sort fields, TODO: manage this to enable multi-sort (splice out if found, then push)
        // (currently only single sort field is supported...)
        $scope.desc = $scope.sortField == f ? !$scope.desc : false; // default to ascending unless second click, then reverse.
        $scope.sortField = f;
    }
})
.config(function ($routeProvider, $locationProvider) {
    // TODO: consider replacing ngRoute with ui-router once it stabilizes...
    // https://github.com/angular-ui/ui-router
    $routeProvider
    .when('/', {
        templateUrl: 'positionDetail.html',
        controller: 'PositionController'
    })
    .when('/Pdf/:pdfId', {
        // TODO: add 'resolve' property to resolve pdfdetail vm before rendering partial:
         templateUrl: appRoot + 'ngpartial/template/PdfDetail',
         controller: 'PdfController',
         resolve: {
             fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                 var id = $route.current.params.pdfId;
                 var getpdf = $http({ // promise
                     method: 'GET',
                     url: appRoot + 'api/PositionDescription/' + id
                 }).success(function (data) {
                     return data.Description;
                 });
                return getpdf;
             }
         }
     });
    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(false);
})
.directive('pbitem', function() {
    alert('pbitem');
})
; // end Angular hr module

// start global JS (outside of AngularJS) functions: (TODO: move these into global.js)
function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

// indexOf Polyfill for IE8:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
        if (this === undefined || this === null) {
            throw new TypeError('"this" is null or not defined');
        }

        var length = this.length >>> 0; // Hack to convert object.length to a UInt32

        fromIndex = +fromIndex || 0;

        if (Math.abs(fromIndex) === Infinity) {
            fromIndex = 0;
        }

        if (fromIndex < 0) {
            fromIndex += length;
            if (fromIndex < 0) {
                fromIndex = 0;
            }
        }

        for (; fromIndex < length; fromIndex++) {
            if (this[fromIndex] === searchElement) {
                return fromIndex;
            }
        }

        return -1;
    };
}
// end Polyfill