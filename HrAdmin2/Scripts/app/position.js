﻿// TODO: combine both position and appt data in AppointmentVm.
//var avm = new AppointmentVm();
//var position; // for caching Position data to pre-populate Fill Level for New Hire

// this retrieves the Appointment view model via AJAX call to api positioncontroller.
// mapped via knockout-mapping
// TODO: handle multi-fills, vacancies, ActionRequests
// TODO: include all tabs in single request (TabsVm)
// TabsVm.OrgUnit, TabsVm.Position, TabsVm.Appointments[n]
// getIncumbents is obsolete...now all contained in position detail.
//function getIncumbents(posId) {
//    // need to set this while tab is visible (in orgTree.js for now)...
//    //$('#statusmessage').text('loading incumbent detail');
//    $.ajax({
//        type: 'GET',
//        url: appRoot + 'api/position/' + posId + '/incumbent',
//        success: function (d) {
//            $('#ApptStatusMessage').text('');
//            // NOTE, currently taking only first incumbent (d[0]) if multi-filled.
//            // TODO: create a tab for each appt in multifill situation.
//            // TODO: include in server response an empty appt VM for New Hire (to keep in sync)
////            var blankVm = {
//////                PersonnelNumber: 11111111,
////                PersonnelNumber: '',
////                FirstName: 'EmpFirst',
////                LastName: 'EmpLast',
////                //EmpId: 0
////                EmpId: ''
//            //            };
////            var arvm = new ActionRequestVm(d.length ? d[0] : undefined);
////            alert(JSON.stringify(d[0]));
//            //            avm = new AppointmentVm(d.length ? d[0] : undefined);
//            if (d[0].EmpId == 0) {
//                d[0].FilledAsJobId = position.JobClassId;
//            }
//            //avm = new AppointmentVm(d[0]);
//            //            ko.applyBindings(avm, $('#detail-appt')[0]); // not binding Contract...
//            //arvm.Appt(avm);
////            ko.applyBindings(avm, $('#detail-appt')[0]); // not binding Contract...
//            // explicitly fire change() here before binding to trigger linked selector:
//            //            $('#ActionType').val(arvm.ActionCode).change();
//            // default to Appointment Change (U3) action:
//            $('#ActionType').val('U3').change();

//            ko.applyBindings(arvm, $('#detail-appt')[0]); // not binding Contract...
//            // explicit contract binding:
//            //$('#Contract').val(avm.ContractTypeCode());

////            ko.applyBindings((d.length ? d[0] : blankVm), $('#detail-appt')[0]);
//        },
//        error: function (xrq, txtStatus, errThrown) {
//            alert(xrq['responseText']);
//            alert(txtStatus);
//        }
//    });
//}

//function getPositionDetail(id) {
//    $('#PosStatusMessage').text('loading position detail');
//    $.ajax({
//        type: 'GET',
//        url: appRoot + 'api/position/' + id,
//        success: function (d) {
//            var position = ko.mapping.fromJS(d.Position);
//            var appt = ko.mapping.fromJS(d.Appointments[0]);
//            $('#PosStatusMessage').text('');
//            $('#ApptStatusMessage').text('');
//            // TODO: bind to tabsVm (mapped vm from d) as root.
//            ko.applyBindings(position, $('#detail-pos')[0]);
//            ko.applyBindings(appt, $('#detail-appt')[0]);
//        },
//        error: function (xrq, txtStatus, errThrown) {
//            alert(xrq['responseText']);
//            alert(txtStatus);
//        }
//    });
//}