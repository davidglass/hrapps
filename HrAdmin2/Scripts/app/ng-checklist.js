﻿angular.module('app', []) // routing module is separate from angular.min.js...


.controller('ChecklistController', function ($scope, $filter, $http, $timeout) {
    $scope.desc = true;
    $scope.sortField = "ParId";

    $scope.isEven = function (index) {
        return (index % 2);
    }

    $scope.sort = function (colname) {
        if (colname == $scope.sortField) { // only reverse direction on second click
            $scope.desc = !$scope.desc;
        }
        else {
            // default to ascending:
            $scope.desc = false;
        }
        //        alert('sorting by ' + colname);
        // TODO: sort function for non-simple string sorts:
        var sorted = $filter('orderBy')($scope.rows, colname, $scope.desc); // simple string sort, reversing
        $scope.rows = sorted;
        $scope.sortField = colname;
    }

    $scope.formatJsonDate = function (d) { return formatJsonDate(d) };

    $scope.load = function () {
        $scope.rows = rows; // embedded server-side by MVC
    }
    $scope.load();

})


.controller('ChecklistDetailController', function ($scope, $filter, $http, $timeout) {

    $scope.vm = vm; // vm global is populated in .cshtml template
    if ($scope.vm.Empchecklist.ParType == 'New Hire') $scope.vm.Empchecklist.UserIdType = 1;

    $scope.formatJsonDate = function (d) { return formatJsonDate(d) };
 




    $scope.save = function (cb){
        var ck = $scope.vm.Empchecklist;
        $scope.asyncOp = true;
        // set message unless already set...
        $scope.StatusMessage = "Saving...";
        $http({
            method: 'PUT',
            url: appRoot + 'api/checklist/' + ck.ParId,
            data: $scope.vm.Empchecklist
        })
        .success(function (data) { // save update returns only validation errors... ignoring for Checklist
            $timeout(function () {
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
                // todo: convert this if to callback..
                if (cb) {
                    cb();
                }
            }, 300);
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.preview = function () {
        window.open(appRoot + 'checklist/' + $scope.vm.Empchecklist.ParId + '/email', "_blank");
    }

    $scope.sendEmail = function () {

        $scope.save(function () {
            // TODO: checkbox for "BCC me", pass val in posted data object...
            $scope.asyncOp = true;
            $scope.StatusMessage = 'sending email';
            $http({
                method: 'POST',
                url: appRoot + 'api/checklist/' + $scope.vm.Empchecklist.ParId + '/email',
                headers: {
                    "Content-Type": "application/json"
                },
                data: $scope.vm.CCL
            })
            .success(function (d) {
                $scope.StatusMessage = 'sent email.';
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.StatusMessage = '';
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncOp = false;
                if (data.error) { alert('ERROR: ' + data.error); };
            });
        });
  
    }
});



// start global JS (outside of AngularJS) functions:
function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

