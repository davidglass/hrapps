﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EmailWindowsService
{
    //To Install: http://msdn.microsoft.com/en-us/library/zt39148a%28v=vs.110%29.aspx
    //On the Start menu or Start Page, open the shortcut menu for Developer Command Prompt, and then choose Run As Administrator.
    //Navigate to the folder that contains your project's output. For example, under your My Documents folder, navigate to Visual Studio 2013\Projects\MyNewService\bin\Debug.
    //Enter the following command: installutil.exe EmailWindowsService.exe

    //Service setup. Used article as starting point, and modified as needed for VS 2013
    //http://www.codeproject.com/Articles/3990/Simple-Windows-Service-Sample

    //to delete: sc delete EmailService

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new EmailService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
