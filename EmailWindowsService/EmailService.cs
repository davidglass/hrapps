﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailWindowsService
{
    public partial class EmailService : ServiceBase
    {
        public EmailService()
        {
            InitializeComponent();
        }

        //using RegisterWaitForSingleObject
        //http://blogs.msdn.com/b/morgan/archive/2008/12/18/periodic-execution-in-net.aspx
        private ManualResetEvent stop = new ManualResetEvent(false);
        private RegisteredWaitHandle registeredWait;

        protected override void OnStart(string[] args)
        {
                stop.Reset();
                registeredWait = ThreadPool.RegisterWaitForSingleObject(stop, new WaitOrTimerCallback(EmailManager), null, (int.Parse(ConfigurationManager.AppSettings["minuteDelay"]) * 60000), false);
        }

        protected override void OnStop()
        {
            stop.Set();
        }

        private void EmailManager(object state, bool timeout)
        {
                
            if (timeout)
            {

                bool success = SendEmails();

                if (!success)
                {
                    //stop any more events coming along
                    registeredWait.Unregister(null);

                    //stop service
                    this.Stop();
                }
                
            }
            else
            {
               

                //stop any more events coming along
                registeredWait.Unregister(null);
            }

                
        }

        //note for if adding attachments
        //http://blog.tutorem.com/post/2011/08/17/Create-a-Simple-Windows-Service-to-Send-Daily-Email-with-Dynamically-Created-CSV-Attachment.aspx

        public bool SendEmails()
        {
            SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["HrAdminDbContext"].ConnectionString);
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["smtpServer"].ToString());

            try
            {

                SqlCommand sqlCommand = new SqlCommand { CommandText = "GetEmails", CommandType = CommandType.StoredProcedure };
                sqlCommand.Connection = cnx;

                DataTable emailsData = new DataTable();

                //read data
                cnx.Open();
                SqlDataReader dr = sqlCommand.ExecuteReader();
                emailsData.Load(dr);
                

                if (emailsData.Rows.Count > 0)
                {
                    foreach (DataRow row in emailsData.Rows)
                    {
                        string exception = "";

                        try
                        {
                            var mFrom = new MailAddress(row["fromAddress"].ToString(), row["fromDisplayName"].ToString());
                            var mTo = new MailAddress(row["toAddress"].ToString(), row["toDisplayName"].ToString());
                            MailMessage email = new MailMessage(mFrom, mTo)
                            {
                                Body = row["body"].ToString(),
                                Subject = row["subject"].ToString(),
                                IsBodyHtml = bool.Parse(row["isBodyHtml"].ToString())
                            };                            

                        
                            smtpClient.Send(email);
                        }
                        catch(Exception e)
                        {
                            //catch smtp message to save to DB
                            exception = e.Message;
                        }

                        //update database that email has been sent
                        var sc = new SqlCommand { CommandText = "SentEmail", CommandType = CommandType.StoredProcedure };
                        sc.Parameters.Add(new SqlParameter("@Id", int.Parse(row["Id"].ToString())));
                        sc.Parameters.Add(new SqlParameter("@Exception", exception));
                        sc.Connection = cnx;
                        sc.ExecuteNonQuery();

                    }
                }

                cnx.Close();
                
            }
            catch(Exception e)
            {
                //likely DB connection issue. Return false to shut down service after sending email.
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpServer"].ToString());
                MailMessage mm = new MailMessage("no-reply@cts.wa.gov", ConfigurationManager.AppSettings["supportEmail"].ToString());
                mm.Subject = "Error with email service.";
                mm.Body = e.Message;
                sc.Send(mm);

                if (cnx.State == ConnectionState.Open)
                    cnx.Close();

                cnx.Dispose();

                return false;
            }
            finally
            {

                if (cnx.State == ConnectionState.Open)
                    cnx.Close();

                cnx.Dispose();
            }
            

            return true;
        }

    }
}
